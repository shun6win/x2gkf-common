# 二鸽在线客服

#### 介绍
二鸽在线客服系统，为您的财富订单添砖加瓦
帮助您在任何网站、app、小程序接入在线客服
提升您的产品的服务满意度
第一时间获取客户的需求
全部 100% 免费
私有部署
易于设置 • 免费 •安全
初创企业 • 个人创业者 • 多站多产品 • 首选

#### 软件架构
软件架构说明


#### 安装教程

请在网站查看
https://x2gkf.com/public/doc/
或者查看部署文档
https://note.youdao.com/s/XdrjjFNW
#### 使用说明

请在网站查看
版本更新，请重新下载源码直接覆盖，重启程序即可

#### 版本更新
##### V1.1.00 (2025-01-09)
*  1、修复商家自定义接待端，消息提醒声音保存问题
*  2、优化接待端，新消息未读条数显示
*  3、优化接待端，新增关闭声音提醒
*  4、专属链接升级为加密地址，升级后如果有直接使用专属链接的，需要重新获取一次。
*  5、PC端客服新增，访客访问记录
*  6、企业版新增，用户自主注册，到期时间管理
*  7、部署方式新增部署界面，无需手动导入数据库，部署地址 http://ip:8090/install 安装后根目录会出现install.lock文件，如需重新安装删除此文件。
*  8、优化接待端 常见问题列表显示
*  9、优化客服端 常见问题列表显示和快捷发送
*  10、PC优化接待端和客服端更多历史记录体验
*  11、PC优化接待端和客服端查看大图体验
*  12、PC优化接待端统计图表
##### V1.0.25 (2024-12-08)
*  1、修复工单系统，在微信和其他个别浏览器无法访问问题
*  2、修复默认主题问题

##### V1.0.23 (2024-09-12)
*  1、新增工单系统
*  2、优化客服接待端显示
*  3、新增客服端我的消息右下角提醒。
*  4、修复 接待端时间不对问题，54年。
*  5、App对应版本V1.1.1
###### V1.0.23 (mysql)
* 1、 表 wolive_warns 新增字段
* ALTER TABLE wolive_warns ADD COLUMN `linkno` char(64) DEFAULT NULL COMMENT '关联id,或者消息id';
* 2、 新表 wolive_tickets
* CREATE TABLE `wolive_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typec` char(255) DEFAULT NULL COMMENT '工单类型',
  `title` varchar(255) DEFAULT NULL COMMENT '工单标题',
  `content` text COMMENT '工单内容json格式{[]}',
  `addtime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '提交时间',
  `status` tinyint(1) DEFAULT '-1' COMMENT '工单状态 0为处理 1已回复 2催单 9已关闭',
  `updatetime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '处理时间',
  `grade` tinyint(1) DEFAULT '0' COMMENT '工单等级 0一般工单 1紧急工单 2非常紧急工单3优先工单',
  `user_type` char(12) DEFAULT 'visiter' COMMENT 'visiter service',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `user_nick` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `service_id` int(11) DEFAULT NULL COMMENT '处理人id',
  `service_nick` varchar(255) DEFAULT NULL COMMENT '处理人昵称',
  `boss_id` int(11) DEFAULT NULL COMMENT '商家id',
  `boss_name` char(64) DEFAULT NULL COMMENT '商家名称',
  `ticket_no` char(36) DEFAULT NULL COMMENT '工单编号',
  `rate` tinyint(1) DEFAULT NULL COMMENT '评分',
  `ratetext` varchar(255) DEFAULT NULL COMMENT '评价',
  PRIMARY KEY (`id`)
  ) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
##### V1.0.22 (2024-06-25)
*  1、新增客服上班时间，在工作时间外用户可以正常留言也可正常接收到消息，但app,pc,email不会进行提醒 设置方式-》客服页面-》右上角我的》基本资料》工作时间
*  2、新增服务开机自启功能（服务器重启后，二鸽客服也会自动重启），linux启动方式不变，windows服务器双击 A启动服务程序.bat  启动服务。
*  3、优化其他功能
##### V1.0.21 (2024-02-20)
*  1、新增客服离线，发送email邮件提醒
*  2、新增访客设置email邮件，如果客服没有及时回复，访客设置了email地址，回复后将会通知
*  3、客服或者客户收到邮箱提醒，可以直接从邮箱链接快捷回复
*  4、接待端新增手机查看二维码
###### V1.0.21 (mysql)
* 1、 表wolive_visiter 删除索引id
* 2、新增表 wolive_visiter_user
* CREATE TABLE `wolive_visiter_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `v_id` char(128) NOT NULL COMMENT '访客id',
  `v_name` varchar(255) NOT NULL COMMENT '用户名',
  `v_nick` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '昵称',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `phone` varchar(255) DEFAULT '' COMMENT '手机',
  `email` varchar(255) DEFAULT '' COMMENT '邮箱',
  `avatar` varchar(1024) NOT NULL DEFAULT '/public/style_js_index/image/defhead/1.png' COMMENT '头像',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1：在线，-1：离线',
  `language` varchar(255) DEFAULT NULL COMMENT '语言',
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `v_id` (`v_id`) USING BTREE
  ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='访客用户表';
##### V1.0.20 (2023-12-19)
*  1、修复客服端可以修改商家管理权限问题
*  2、优化其他功能
##### V1.0.19 (2023-12-05)
* 1、超级后台新增版本自动检测；登录后即可自动检测一键升级，可忽略
* 2、新增app版本更新，后台配置后电脑端和app都会提示更新信息一键更新
* 3、超级后台优化调整
* PS:服务器、app、电脑端自此版本后，更新都可以一键更新。
##### V1.0.18 (2023-11-25)
* 1、新增接待页面，发送文件、视频开关
* 2、新增接待页面，全屏模式；设置方式-》客服页面-》商家设置
* 3、新增接待页面，右侧广告；设置方式-》客服页面-》商家设置
* 4、电脑端发消息给用户，app不同步问题
* 5、后台新增，关闭首页配置：设置方式-》超级管理员后台-》站点信息配置
* 6、接待端优化，一个客户访问同一商家不同客服主号，不保留信息问题；
* 7、新增域名黑白名单：设置方式-》超级管理员后台-》域名黑白名单
###### V1.0.18 (mysql)
* 1、 表wolive_visiter 删除索引id
##### V1.0.17 (2023-11-14)
* 1、新增接待页面，小圆点未读消息提示
* 2、优化接待端和pc端查看大图功能
* 3、后台新增站点信息配置、logo、标题等
* 4、超级管理员后台新增，清空聊天文件、聊天图片等信息
* 5、关键词机器人自动回复,设置方式：接待端-》常见问题，选择关键词，填写相关关键词
* 6、访客端新增结束会话
* 7、接待端新增未回复提示
* 8、新增 主客服和客服小二，客服小二就是主客服的子账号，可以同时接待主客服所有访客
* 设置方式-》登入主客服-》右上角客服昵称-》管理客服小二
###### V1.0.17 (mysql)新增
* 1、 新增wolive_config表
* CREATE TABLE `wolive_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '配置标题',
  `key` char(64) NOT NULL COMMENT '配置key',
  `value` text COMMENT '配置value',
  `isjson` tinyint(1) DEFAULT '0' COMMENT '是否json',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `configkey` (`key`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
* 2、 表wolive_visiter 新增字段 `last_msgfrom` varchar(255) DEFAULT NULL COMMENT '最后一条
* 3、 表wolive_msgs 新增字段
* `fnick` varchar(255) DEFAULT NULL COMMENT '发送者昵称',
  `fhead` varchar(255) DEFAULT NULL COMMENT '发送者头像',
  `extend` text COMMENT '扩展信息',
* 4、 表wolive_quickmsg 新增字段
* `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关键词'
* 可以选择重新导入数据库文件
* [online.service.golang.sql](online.service.golang.sql)
##### V1.0.16 (2023-11-01)
* 1、新增自定义后台登入地址，配置文件config.ini ->"AdminLoginUrl":"service/login"
* 2、新增好友功能，可添加客服好友
* 3、优化手机访客接待端，弹窗模式时窗口太小的问题
* 4、去除客服端，在线客服链接
* 更新影响提示：更新后之前聊天图片和文件将无法访问，客服和商家图片需重新上传。
* 原因：为方便后期在后台可直接清空聊天图片和文件，做了目录结构调整
* 电脑端exe V1.0.16
* ----新增新消息弹窗提示，新增快捷回复，新增托盘消息闪烁提醒，新增未读消息数量提示
* 安卓app V1.0.8 2023-11-07更新，直接前往官网重新下载
* ----优化推送问题，优化偶尔进入消息不更新问题，新增添加好友，新增自定义后台配置
##### V1.0.15 (2023-10-19)
* 1、后台新增翻译接口配置，登入root账号后进行配置
* 2、访客端新增语言选择，页面翻译
* 3、后台接待页面新增访客翻译语言修改，访客语言与客服语言不一致自动出现翻译功能
##### V1.0.14 (2023-09-05)
* 1、新增访客记录清空，访客聊天记录清空，清空方式->后台接待右上角 商家设置 滑动到底部
* 2、优化访客接待页面，发送按钮被隐藏问题
* 3、后台接待页面新增近期、在线、姓名分类
##### V1.0.13 (2023-09-02)
* 1、新增app客户端链接编号设置，设置方法->后台登入root超级管理员账号->选择 服务器编号设置->配置站点标题和域名->提交生成
##### V1.0.12 (2023-08-19)
* 1、新增接口预计提醒推送，设置方法->我的信息修改中查看修改，支持加密提交提醒。


