//聊天框粘贴发送
document.onpaste = function (event) {
    if ($(event.target).hasClass('el-textarea__inner')) {
        let items = (event.clipboardData || event.originalEvent.clipboardData).items;
        let item = items[items.length - 1];

        if (item.kind === 'file') {
            var file = item.getAsFile();
            if (item.type.indexOf("image/") != -1) {
                vueapp.uploadImgAndFilePost(Nginxpath+"/api/uploadimg", file, "imgfile", "img");
            } else {
                if(vueapp.bossinfo.AudioState==1){
                    vueapp.uploadImgAndFilePost(Nginxpath+"/api/uploadfile", file, "file", "file");
                }

            }
        }
    }
};

//浏览器通知
var notify = new Notify({
    effect: "flash",
    title:"客户接待中心",
    interval: 500,
    audio: {
        file:[]
        //file: "/public/style_js_index/default.mp3",
    },
});



var vueapp = new Vue({
    el: '#app',
    data: {
        conn: {},
        msgList: [],
        msgListPage:{
            LastPage:1,
            Limit:15,
            Page:1,
            Total:0
        },
        windowtype: getQueryString('type'),//pcmin
        theme: getQueryString('theme'),//主题色
        ticketsreadnum:0,//  未读工单
        ToId: windowToId,//客服id
        visitor_id: '',
        boss_id: windowBossId,
        showLoadMore: false,
        showUserInput: "",
        msgpage: 1,
        limit: 15,
        quicklist: [],//热门问题
        quickKeyWorklist: [],//热门问题关键词检测
        quicklistShow: false,
        showExtendQuick: false,

        MsgNoAname: '',//消息定位用
        showIconBtns: false,
        messageContent: "",
        sendDisabled: false,
        showExtendTool: false,
        showLanguage: false,

        languagetype: "zh",
        languageList: {},
        bossinfo: {
            "Id": 1,
            "BossName": "",
            "AdminId": 0,
            "Logo": "",
            "Copyright": "",
            "VideoState": 1,
            "VoiceState": -1,
            "AudioState": 0,
            "TemplateState": 0,
            "DistributionRule": 0,
            "VoiceAddress": "",
            "KefuGonggao": "",
            "Remark": "",
            "Msgreadday": 1,
            "OverTime": 0,
            "MaxCount": 0,
            "PushUrl": "",
            "Ads":"",
            "Status": -1
        },
        serviceinfo: {
            "ServiceId": 1,
            "UserName": "",
            "NickName": "",
            "Password": "",
            "Groupid": "",
            "Phone": "",
            "ServiceType": 0,
            "Email": "",
            "BossIds": "",
            "Avatar": "/public/style_js_index/image/defhead/4.png",
            "OfflineFirst": 0,
            "State": 1,
            "Worktimejson": ""
        },
        sendFaceIconList: [],
        chatBoxSendCloseShow: false,
        VisitorVoiceBtn: 'false',
        VisitorMapBtn: '',
        VisitorCommentBtn: '',
        showFaceIcon: false,
        VisitorReadStatus: 'true',
        VisitorPlusBtn: 'false',
        VisitorUploadImgBtn: 'false',
        VisitorUploadFileBtn: 'false',
        VisitorMaxLength: '100' == '' ? 100 : parseInt('100'),
        VisitorShowAvator: 'false',
        sendPingTime: 5,
        sendPingTimeDef: 5,
        VisiterName: "",
        Visitertoken:"",
        showRightTop:false,
        readMsgnum:0,
        isnotifyMp3:true

    }, mounted: function () {

    },
    created: function () {
        var _this = this;
        var vid = getCookie("visitor_id" + _this.boss_id);
        var isemail = getQueryString("isemail");
        if (vid == "" || vid == undefined || isemail=="1") {
            vid = getQueryString('v_id');
        }else {
            //自定义 vid 加前缀区分
           /* if(vid.indexOf( _this.boss_id+"z-")!=0){
                vid = _this.boss_id+"z-"+vid;
            }*/
        }
        _this.VisiterName = getQueryString('v_name');
        if((this.theme + "")==""){
            this.theme = "#00BF6D";
        }else if ((this.theme + "").slice(0, 1) != "#") {
            this.theme = "#" + this.theme;
        }

        if (vid != "") {
            _this.connIni(vid)
        } else {
            var fpPromise = FingerprintJS.load()
            fpPromise
                .then(fp => fp.get())
                .then(result => _this.connIni(_this.boss_id+"-"+ result.visitorId))
        }
_this.getyuyanlist();

    },
    methods: {
        connIni(visitor_id) {

            var _this = this;
            _this.visitor_id = visitor_id


            _this.wsLogin(function (data) {
                _this.VisiterName = data.VisiterName;
                _this.Visitertoken = data.Visitertoken;
                _this.visitor_id = data.VisiterId;
                setCookie("visitor_id" + _this.boss_id, _this.visitor_id);
                _this.languagetype = data.language;
                _this.bossinfo = data.bossinfo;


                /*if(isMobile()){
                    //如果是手机端，这里直接设置100%
                    //_this.bossinfo.TemplateState="max-width: 900px;max-height: 780px;";
                    _this.bossinfo.TemplateState="max-height: "+window.innerHeight+"px";
                }else{
                    _this.bossinfo.TemplateState=_this.bossinfo.TemplateState==-1?'max-width: 100%;max-height: 100%;':'max-width: 900px;max-height: 780px;';
                }*/
                if (_this.bossinfo.VoiceAddress!="") {
                    notify.setURL(Nginxpath+_this.bossinfo.VoiceAddress)
                }
                _this.serviceinfo = data.serviceinfo;
                if (data.quicklist){
                    _this.quicklist = data.quicklist;
                    for (var i = 0; i < data.quicklist.length; i++) {
                        if (data.quicklist[i].Indexc >= 0) {
                            _this.quicklistShow = true;
                        }
                        //关键词
                        if(data.quicklist[i].Keyword!=""){
                            _this.quickKeyWorklist.push({"Keywords":data.quicklist[i].Keyword.split(","),"data":data.quicklist[i]});
                        }
                    }
                }


                document.title = _this.bossinfo.BossName + '_接待中心';
               // notify.setTitle(document.title);
                _this.webSocketInit();
                _this.apigetmsg()
                // window 每次获得焦点
                window.onfocus = function () {
                    _this.MsgReadFunc();
                };

                 setInterval(function (){
                     _this.sendPingTime--;
                     if(_this.sendPingTime<0){
                         _this.sendPingTime = _this.sendPingTimeDef;
                         _this.sendPing();

                     }

                 },1000)

                if (_this.languagetype != "zh" && _this.languagetype != "" && viptf("fy")) {
                    setTimeout(function () {
                        apiTranslateHtml('.bodytramslate','auto_'+_this.languagetype);
                    }, 1000)
                }

                //XiaoChengKFfloatTitle
                //悬浮框的标题显示
                //子页面发送：
                window.parent.postMessage({ title: _this.bossinfo.BossName }, '*')

                _this.myTicketsreadnum();

            });
        },
        webSocketInit() {
            var _this = this;
            if (window["WebSocket"]) {
                _this.conn = new WebSocket(getWsUrl() +Nginxpath5+ "/ws?from=" + _this.visitor_id);
                _this.conn.onopen = _this.OnOpen;
                _this.conn.onclose = _this.OnClose;
                _this.conn.onmessage = _this.OnMessage;
            } else {
                console.log("Your browser does not support WebSockets");

            }
        },
        OnOpen: function () {
            this.chatBoxSendCloseShow = false;
            this.sendInputingStrNow("访客进入", "text", "login")
            //window.location.reload();
        },
        OnMessage: function (evt) {
            if (evt.data == '{"T":"po"}') {
                return;
            }
            var _this = this;
            var messages = JSON.parse(evt.data);

            var _mkey=msgMkey(messages.D.ToId,messages.D.FromId);
            //多商家版过滤，不是这个商家的 不显示
            if (_mkey!=msgMkey(_this.visitor_id,_this.ToId)) {
                return;
            }

            if (messages.T == "msg") {
                _this.msgList.push(messages.D)
                _this.scrollBottom();

                if (messages.D.FromId != _this.visitor_id ) {
                    _this.readMsgnum++;
                    notify.setTitle("有"+this.readMsgnum+"条新消息！");


                    notify.setFavicon(_this.readMsgnum)

                    if (_this.isnotifyMp3) {
                        notify.player();
                    }
                    //悬浮框的消息条数显示，小红点
                    window.parent.postMessage({ msgnum:_this.readMsgnum}, '*')

                }
                buildImagelookBigImg(".chatMsgContent");
                //_this.iswindowPmFunc()
            } else if (messages.T == "recall") {//消息测回
                var reindex = -1;
                for (index in _this.msgList) {
                    if (_this.msgList[index].MsgNo == messages.D.Content) {
                        reindex = index;
                        break
                    }
                }
                if (reindex > -1) {
                    _this.msgList.splice(reindex, 1);
                }

            } else if (messages.T == "read") {//消息已读
                for (index in _this.msgList) {
                    if (_this.msgList[index].FromId == _this.visitor_id) {
                        _this.msgList[index].Isread = 1;
                    }

                }
            } else if (messages.T == "input") {
                _this.showUserInput = messages.D.Content;

            }


        },
        OnClose: function (evt) {
            var _this = this;
            _this.chatBoxSendCloseShow = true;
            setTimeout(function () {
                _this.webSocketInit();

            }, 5000)
        },
        MsgReadFunc() {//标记消息已读
            var _this = this;

            var sendread = false;
            if (!document.hidden) {//当前屏幕
                for (var k in _this.msgList) {
                    if (_this.msgList[k].FromId == _this.ToId && _this.msgList[k].Isread != 1) {
                        _this.msgList[k].Isread = 1;
                        sendread = true;
                    }

                }
                if (sendread) {
                    _this.sendInputingStrNow("1", "text", "read")
                    notify.setTitle("");
                    notify.setFavicon("");

                }

                //悬浮框的消息条数显示，小红点
                _this.readMsgnum=0;
                window.parent.postMessage({ msgnum:0}, '*')
            }

        }, sendPing() {
            if (this.chatBoxSendCloseShow) {
                return;
            }
            this.conn.send('{"T":"pi"}');
        },
        MsgTp() {
            return {
                "T": "msg",
                "D": {
                    "MsgType": "text",
                    "MsgNo": "1",
                    "Mkey": this.ToId + "|" + this.visitor_id,
                    "FromId": this.visitor_id,
                    "ToId": this.ToId,
                    "BossId": this.boss_id,
                    "Content": "",
                    "Content2": "",
                    "Fhead": "",
                    "Fnick": this.VisiterName,
                    "Ftime": "",
                    "Isread": -1,
                    "Extend": ""
                }
            };
        },
        wsLogin(func) {
            var _this = this;
            var _Referer = getCookie("Referer");
            var _avatar = getCookie("visitoravatar");
            _Referer = _Referer ? _Referer : document.referrer;
            DianJinAjax(_this.boss_id, {dataCode: false, resCode: true}, {
                url: Nginxpath+'/api/visitor/login',
                type: "post",
                data: {
                    "to_id": _this.ToId
                    , "visitor_id": _this.visitor_id
                    , "boss_id": _this.boss_id
                    , "Referer": _Referer ? _Referer : window.location.href
                    , "visitor_name": _this.VisiterName
                , "avatar": _avatar
                    , "zdy1": getQueryString('zdy1')
                    , "zdy2": getQueryString('zdy2')
                    ,"language": navigator.language
                    , "extends": JSON.stringify({
                        "language": navigator.language
                        , "userAgent": navigator.userAgent
                        , "os": getOSAndDeviceInfo(navigator.userAgent)
                        , "browserName": myBrowser(navigator.userAgent)
                    })

                },
                success: function (res) {
                    if (res.code == 200) {
                        func(res.data);
                    } else {
                        layer.alert(res.msg);
                    }
                }
            })





                if(window.location.href.indexOf("/service/setlink?")==-1 && getQueryString('type')!="pcmin" ){

                $.ajax({
                    url: Nginxpath+'/api/visitor/addlog',
                    type: "post",
                    data: {
                        Referer:_Referer ? _Referer : window.location.href
                        , "visiter_id": _this.visitor_id
                        , "boss_id": _this.boss_id
                        ,title:"访客访问："+document.title
                    },
                    success: function (res) {

                    },
                    error: function (data) {
                    }
                });
                }
        },

        selectQuick(msg,isstring=false,islayeropen=false) {
            if (isstring){
                var _dj=DianJin_decode(msg,"msg");
                //console.log(_dj)
                //_dj=HTMLDecode(_dj);
                msg=JSON.parse(_dj);
            }

            // 新页面打开
            if(msg.State==3 || islayeropen){
                layer.open({
                    title:"",
                    type:1,
                    area: ['80%', '80%'], //宽高
                    content:'<div class="selectQuicklayer_open" style="padding: 5px"><h2 style="padding: 10px;text-align: center">'+msg.Title+'<hr></h2>'+msg.Content.replace(/\\\"/g,'"')+'</div>',
                    success: function (layero, index) {
                        buildImagelookBigImg(".selectQuicklayer_open");
                    }
                });
                return;
            }

            var _this = this;
            _this.sendInputingStrNow(msg.Title, 'text', 'msg',1);
            setTimeout(function () {
                var message = _this.MsgTp();
                message.T = 'msg';
                message.D.ToId = message.D.FromId;
                message.D.FromId = _this.ToId;
                message.D.Fnick = "机器人";
                message.D.Isread = 1;
                message.D.Fhead = "/public/style_js_index/image/defhead/reboot.png";
                message.D.Content = DianJin_encode(msg.Content, _this.boss_id);
                message.D.MsgType = 'text';
                _this.conn.send(JSON.stringify(message));
            }, 800)
        },
        checkQuickKeyWork(msg) {
            msg=msg+"";

            if(msg.indexOf("人工")!=-1){
                return "";
            }

            var _titles=[];
            var _content="";
            var _this=this;
            for (var i=0;i< _this.quickKeyWorklist.length;i++){
                var _data=_this.quickKeyWorklist[i];
                for (var i2=0;i2<_data.Keywords.length;i2++){
                    if (msg.indexOf(_data.Keywords[i2])!=-1){
                        _content=_data.data.Content;
                        _titles.push('<a class="slideInRightItem" onclick="vueapp.selectQuick(\''+DianJin_encode(JSON.stringify(_data.data),"msg")+'\',true)" >'+(_titles.length+1)+'、'+_data.data.Title+'</a>');

                    }
                }
            }

            if(_titles.length==0){
                return "";
            }
            if(_titles.length==1){
                return _content;
            }

                return '以下是根据您的提问匹配的相关问题：<br>'+_titles.join("<br>");

        },
        chatToUser: function (e) {
            var _this=this;
            if (this.sendDisabled || this.messageContent == "") {
                return;
            }

            if (e && e.ctrlKey && e.keyCode == 13) {
                this.messageContent += '\n';
                return;
            }

            if (e && e.keyCode == 13 && this.messageContent.slice(-1) == "\n") {
                //去除回车自动加的\n
                this.messageContent = this.messageContent.toString().slice(0, this.messageContent.length - 1);
            }

            this.sendInputingStrNow(SendTextEncode(this.messageContent), 'text')

            //关键词检查回复
            var checkmsg=_this.checkQuickKeyWork(this.messageContent);
            if(checkmsg){
                setTimeout(function () {
                    var message = _this.MsgTp();
                    message.T = 'msg';
                    message.D.ToId = message.D.FromId;
                    message.D.FromId = _this.ToId;
                    message.D.Fnick = "机器人";
                    message.D.Fhead = "/public/style_js_index/image/defhead/reboot.png";
                    message.D.Content = DianJin_encode(checkmsg, _this.boss_id);
                    message.D.MsgType = 'text';
                    _this.conn.send(JSON.stringify(message));
                }, 800)
            }



            this.messageContent = "";
            this.showFaceIcon = false;
            this.updateAsk();
        },
        sendFaceIconListb() {
            var _this = this;
            _this.showFaceIcon = !_this.showFaceIcon;
            if (_this.sendFaceIconList.length > 0) return;
            for (var i = 0; i < 43; i++) {
                _this.sendFaceIconList.push('/public/style_js_index/image/faces/' + i + '.png')
            }
        },
        sendFaceIcon(facetext) {
            this.messageContent += facetext
        },
        inputNextText: function (e) {

            if (!this.conn || this.messageContent == "" || this.messageContent == "\n") {
                return;
            }
            var _this = this;
            _this.sendDisabled = false;
            _this.sendInputingStrNow(SendTextEncode(this.messageContent), 'text', 'input');
        },
        recallMsg(MsgNo) {
            var message = this.MsgTp();
            message.T = 'recall';
            message.D.Content = MsgNo;
            message.D.MsgType = 'text';
            this.conn.send(JSON.stringify(message));
        },
        sendInputingStrNow(str, msgtype = "text", messageT = "msg",Isread=0) {
            if (!this.conn || str == "" || str == "\n" || this.boss_id < 1) {
                return;
            }
            var message = this.MsgTp();
            message.T = messageT;
            message.D.Content = DianJin_encode(str, this.boss_id);
            message.D.MsgType = msgtype;
            message.D.Isread =Isread;
            this.conn.send(JSON.stringify(message));
        },
        updateAsk() {
            var time = new Date().toLocaleDateString();
            var time2 = getCookie("updateAsk");
            if (time2==time){
                return;
            }
            //console.log(time.toLocaleDateString())   //打印结果为：2022/8/31

            DianJinAjax(this.boss_id, {dataCode: false, resCode: true}, {
                url: Nginxpath+'/api/visitor/askupdate',
                type: "POST",
                data: {"boss_id":this.boss_id},
                success: function (res) {
                    setCookie("updateAsk",time);
                }
            });
        },
        getyuyanlist(){
            var _this=this;
            $.ajax({
                url: Nginxpath+'/public/style_js_index/fanyiconfig.json',
                type: "get",
                data: {},
                success: function (res) {
                    _this.languageList=res;
                   /* if(!_this.languageList[_this.languagetype]){

                        layer.alert("客服语言标识【"+_this.languagetype+"】不存在，请在右上角客服昵称下拉修改基础资料中修改语言，现已为您切换到默认【简体中文 zh】")
                        _this.languagetype='zh';
                    }*/
                }
            });
        },
        setLanguage(type) {
            this.showLanguage = false;
            if (viptf("fy"))return;
            if (this.languagetype != "zh" && type == "zh") {

                setTimeout(function () {
                    window.location.reload();
                }, 1000)
            } else {
                this.languagetype = type;
                setTimeout(function () {
                    apiTranslateHtml('.bodytramslate','auto_'+type);
                }, 1000)
            }
            DianJinAjax(this.boss_id, {dataCode: false, resCode: true}, {
                url: Nginxpath+'/api/visitor/language',
                type: "POST",
                data: {"language": type, "visitor_id": this.visitor_id},
                success: function (res) {

                }
            });
        },
        scrollBottom: function () {
            var _this = this;
            this.$nextTick(function () {
                var container = _this.$el.querySelector(".chatVisitorPage");
                container.scrollTop = 999999;

            });
        },
        textareaFocus: function () {
            this.MsgReadFunc();
            this.scrollBottom();
        },
        textareaBlur: function () {
            this.scrollBottom();
        },
        faceIconClick: function (index) {
            // this.showFaceIcon=false;
            this.messageContent += "face" + this.face[index].name;
        },
        apigetmsgPage(){
            var _this=this;
            if(_this.msgListPage.LastPage<=_this.msgListPage.Page){
                return;
            }
            _this.apigetmsg(_this.msgListPage.Page+1);
        },
        apigetmsg(msgpage=1) {
            var _this = this;
            DianJinAjax(_this.boss_id, {dataCode: false, resCode: true}, {
                url: Nginxpath+'/api/visitormsg',
                type: "get",
                data: {
                    "from": _this.ToId,
                    "to": _this.visitor_id,
                    page: msgpage,
                    limit: _this.limit,
                    "day": _this.bossinfo.Msgreadday
                },
                success: function (res) {
                    if (res.code == 200 && res.data.data && res.data.data.length>0) {
                        _this.msgListPage=res.data.page;
                        var _lsmsg=[];
                        if (res.data.page.Page!=1){
                            _lsmsg=_this.msgList;
                        }
                        for (index in res.data.data){
                            _lsmsg.unshift(res.data.data[index])
                        }
                        _this.msgList=_lsmsg;
                            setTimeout(function () {
                                var aks = res.data.data[0].MsgNo
                                var el = document.getElementById(aks);
                                if(el){
                                    el.scrollIntoView({
                                        behavior: 'smooth', // 平滑滚动
                                        block: 'start'      // 滚动到视图的顶部
                                    });
                                }
                            }, 100)
                    }
                    // 添加公告 和 首次问候语
                    if (_this.msgListPage.Page ==1) {
                        setTimeout(function () {

                            //这里加入公告
                            if (_this.bossinfo.KefuGonggao != "") {
                                var msgd = _this.MsgTp();
                                //msgd.D.Content=DianJin_encode(_this.bossinfo.KefuGonggao,_this.boss_id)
                                msgd.D.Content = (_this.bossinfo.KefuGonggao)
                                msgd.D.MsgType = "gonggao";
                                _this.msgList.push(msgd.D);
                                _this.scrollBottom();
                            }

                            // 如果客服不在线
                            var msgd = _this.MsgTp();
                            if(_this.serviceinfo.Worktimejson==""){
                                //这里加入首次问候语
                                if (_this.bossinfo.Firstmsg != "") {
                                    var _msg = _this.bossinfo.Firstmsg.replace(/{NickName}/g, _this.serviceinfo.NickName);
                                    _msg = _msg.replace(/{ServiceId}/g, _this.serviceinfo.ServiceId);
                                    msgd.D.Content = DianJin_encode(_msg, msgd.D.BossId)
                                    //msgd.D.Content=(_this.bossinfo.KefuGonggao)

                                }else{
                                    msgd.D.Content =  DianJin_encode("很高兴为您服务，请问有什么可以帮您？", msgd.D.BossId)
                                }
                                msgd.D.Fnick = _this.serviceinfo.NickName;
                            }else {
                                msgd.D.Fnick = "机器人";
                                msgd.D.Content =  DianJin_encode(_this.serviceinfo.Worktimejson, msgd.D.BossId)
                            }


                            msgd.D.ToId = msgd.D.FromId;
                            msgd.D.FromId = _this.ToId;

                            let time = new Date(new Date().getTime()+window.ServerTimeUnix);
                            msgd.D.Ftime = FormatTimeToSystem(time.toLocaleString());

                            _this.msgList.push(msgd.D);
                            setTimeout(function () {
                                _this.scrollBottom();
                            }, 500)

                        }, 500)
                    }
                    buildImagelookBigImg(".chatMsgContent");
                },
                error: function (data) {


                }
            });
        },
        uploadImgAndFilePost(url, file, postfilename = "file", type = "img") {
            var _this = this;
            var formData = new FormData();
            formData.append(postfilename, file);
            var indexload = layer.load(3);
            $.ajax({
                url: url || '',
                type: "post",
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                mimeType: "multipart/form-data",

                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.upload.onprogress = function (e) {
                        var w = parseInt((e.loaded / e.total) * 100)
                        _this.percentage = w;
                        if (w >= 100) {
                            _this.percentage = 0;
                        }
                    }
                    return xhr
                },
                success: function (res) {
                    layer.close(indexload);
                    if (res.code != 200) {

                    } else {
                        _this.sendInputingStrNow(res.data, type)
                        setTimeout(function () {
                            _this.scrollBottom();
                        }, 2000);
                    }
                },
                error: function (data) {

                    layer.close(indexload);
                }
            });
        },
        uploadImg: function (url) {
            let _this = this;
            _this.showExtendTool = false;
            $('#uploadImg').after('<input type="file" accept="image/gif,image/jpeg,image/jpg,image/png" id="uploadImgFile" name="file" style="display:none" >');
            $("#uploadImgFile").click();
            $("#uploadImgFile").change(function (e) {
                var file = $("#uploadImgFile")[0].files[0];
                _this.uploadImgAndFilePost(url, file, "imgfile", "img");
            });
        },

        uploadFile: function (url) {
            let _this = this;
            _this.showExtendTool = false;
            $('#uploadFile').after('<input type="file"  id="uploadRealFile" name="file2" style="display:none" >');
            $("#uploadRealFile").click();
            $("#uploadRealFile").change(function (e) {
                var file = $("#uploadRealFile")[0].files[0];
                _this.uploadImgAndFilePost(url, file, "file", "file");
            });
        },
        lookEwm(){
            var _this=this;
            layer.open({
                title:"手机扫一扫",
                type:1,
                area: ['260px', '310px'], //宽高
                content:"<div id=\"qrcode\"></div>",
                success: function (layero, index) {
                    new QRCode(document.getElementById("qrcode"), window.location.href+(window.location.href.indexOf("?")==-1?'?':'&')+"v_id="+_this.visitor_id);  // 设置要生成二维码的链接
                }
            });
        },
        editVisiterEmail: function (){
//prompt层
            var _this=this;
            layer.prompt({title: '输入您的email邮箱地址，用于对话关闭后接收回复的消息！', formType: 2}, function(pass, index){
                if((pass+"").indexOf("@")==-1){
                    layer.msg("请填写正确格式的email邮箱地址");
                    return;
                }
                DianJinAjax(1, {dataCode: true, resCode: true}, {
                    url: Nginxpath+'/api/visitor/setemail',
                    type: "post",
                    data: {
                        "email": pass,
                        "token": _this.Visitertoken,
                        "visitor_id": _this.visitor_id,
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            layer.close(index);
                        }
                        layer.msg(res.msg);
                    },
                    error: function (data) {


                    }
                });


            });
        },addTickets: function (){
            var _this=this;
            layer.open({
                type: 2
                ,title: '提交新工单'
                ,content: Nginxpath+'/tickets/from?boss_id='+_this.boss_id
                ,area: ['90%', '90%']
                ,maxmin: true
                ,btn: [ '取消']
                ,yes: function (index) {
                    layer.close(index); //关闭弹层
                }
            });
        },myTicketsList: function (){
            var _this=this;
            layer.open({
                type: 2
                ,title: '我的工单'
                ,content: Nginxpath+'/tickets/list?boss_id='+_this.boss_id
                ,area: ['100%', '100%']
                ,maxmin: true
                ,btn: [ '取消']
                ,yes: function (index) {
                    layer.close(index); //关闭弹层
                    _this.myTicketsreadnum();
                }
            });
        },myTicketsreadnum: function (){
            var _this=this;
            DianJinAjax(1, {dataCode: false, resCode: false}, {
                url:  Nginxpath+'/tickets/list/readnum?boss_id='+_this.boss_id,
                type: "get",
                data: {

                },
                success: function (res) {
                    if (res.code == 200) {
                        _this.ticketsreadnum=res.data;
                    }

                },
                error: function (data) {


                }
            });

        },openMinQuick(){
            var _this=this;
            var content='';
            var ik=0;
            for (var i=0;i<_this.quicklist.length;i++){
                var msg=_this.quicklist[i];
                if((msg.State==1 || msg.State==3) && msg.Indexc>-1){
                    ik++;
                    content+='<li class="chatArticleItem" onclick="vueapp.selectQuick(\''+DianJin_encode(JSON.stringify(msg),"msg")+'\',true,true)"><a class="bodytramslate" style="color: #0f88e3">'+ik+'、'+msg.Title+'</a><hr></li>'
                }
            }


            layer.open({
                type: 0
                ,title: '💢常见问题'
                ,content: content
                ,area: ['100%', '100%']
                ,maxmin: false
                ,btn: [ '取消']
                ,yes: function (index) {
                    layer.close(index); //关闭弹层
                    _this.myTicketsreadnum();
                }
            });
        }

    },

})

//接收iform 过来的 消息：
window.addEventListener('message', function (event) {
    if (event.data=="editVisiterEmail"){
        vueapp.editVisiterEmail();
    }else  if (event.data=="lookEwm"){
        vueapp.lookEwm();
    }
})
