
var  configjson={
    "Title":"APP最新版本更新信息",
    "Cvalue":{},
    "Isjson":1,
    "Ckey":"setvarupdateapp",
    "Remark":"",
}

var $, form;
var upload;
var configkey = configjson.Ckey;

layui.config({
    base: '../../public/style_js_admin/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'upload', 'form'], function(){
    $ = layui.jquery
        ,form = layui.form;
    upload = layui.upload;



    if(configkey){
        layer.load(1)

        DianJinAjax("1",{dataCode: false,resCode: false},{
            url: '/admin/configget',
            type: 'GET',
            data: {configkey: configkey},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值
                    if(result.data.Cvalue!=""){
                        var jsonk=JSON.parse(result.data.Cvalue);
                        if(jsonk){
                            form.val("layuiadmin-form-role",jsonk);
                        }
                    }


                    //

                   /* if(result.data.Btnjson!=""){
                        vueapp.btns=JSON.parse(result.data.Btnjson);
                    }

                    $("#logoimg").attr("src",result.data.Logo);
                    jQuery('#summernote').summernote('code', result.data.KefuGonggao);*/
                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }

    form.on('submit(LAY-user-role-submit)', function(data){
        var field = data.field; //获取提交的字段

        configjson.Cvalue=JSON.stringify(field);
        //提交 Ajax 成功后，静态更新表格中的数据
        DianJinAjax("1",{dataCode: true,resCode: false},{
            url: '/admin/configset',
            type:'POST',
            data:configjson,
            success:function(result){
                layer.msg(result.msg)
                if(result.code==200){
                    window.location.reload();
                }

            }});

    });

var loginlogoimgindex;
    //同时绑定多个元素，并将属性设定在元素上
    upload.render({
        elem: '.demoMorewin'
        ,url:'/api/uploadfile'
        ,accept: 'file'
        ,data:{name:"",path:"config/varupdateapp/","md5":1}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
            loginlogoimgindex=layer.load(1);
        }
        ,done: function(res, index, upload){
            layer.close(loginlogoimgindex);
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#windownurl").val(window.location.origin+res.data);
            $("#windownmd5").val(res.md5);
        }
    })

    upload.render({
        elem: '.demoMorean'
        ,url:'/api/uploadfile'
        ,accept: 'file'
        ,data:{name:"",path:"config/varupdateapp/","md5":1}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
            loginlogoimgindex=layer.load(1);
        }
        ,done: function(res, index, upload){
            layer.close(loginlogoimgindex);
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#andownurl").val(window.location.origin+res.data);
            $("#andownmd5").val(res.md5);
        }
    })

    upload.render({
        elem: '.demoMoreios'
        ,url:'/api/uploadfile'
        ,accept: 'file'
        ,data:{name:"",path:"config/varupdateapp/","md5":1}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
            loginlogoimgindex=layer.load(1);
        }
        ,done: function(res, index, upload){
            layer.close(loginlogoimgindex);
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#iosdownurl").val(window.location.origin+res.data);
            $("#iosdownmd5").val(res.md5);
        }
    })

});


function tbX2gAPPupdate() {
    DianJinAjax("1",{dataCode: false,resCode: false},{
        url: '/admin/verupdateapp',
        type: 'GET',
        data: {configkey: configkey},
        success: function (result) {
            if (result.code == 200) {
                form.val("layuiadmin-form-role",result.data);
                layer.alert("已同步，填写完成，请查看确认无误后，点击提交 （ps：提交前，请先确定服务器代码是否和官网一致为最新版，服务器编号设置 中进行检测更新查看）")
            } else {
                layer.msg(result.message)
            }

        }
    });
}