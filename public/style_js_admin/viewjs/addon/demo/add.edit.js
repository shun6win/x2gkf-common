

var $, form;
var upload;

layui.config({
    base: '../../public/style_js_admin/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'upload', 'form'], function(){
    $ = layui.jquery
        ,form = layui.form;
    upload = layui.upload;
    var datatypeintarr = $("#inputtypeints").val().split(",");
    ($("input").each(function (index,e) {
        if($(e).attr("data-type") && $(e).attr("data-type")=="int"){
            datatypeintarr.push($(e).attr("name"))
        }
    }))
    $("#inputtypeints").val(datatypeintarr.join(","));


    var id = getQueryString('id');
    if(id>0){
        layer.load(1)

        DianJinAjax("1",{dataCode: false,resCode: true},{
            url: './info',
            type: 'GET',
            data: {id: id},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值

                    form.val("layuiadmin-form-role",result.data);

                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }



    //同时绑定多个元素，并将属性设定在元素上
    upload.render({
        elem: '.demoMoref'
        ,url:'/api/uploadfile'
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#mp3").val(res.data);
        }
    })

    upload.render({
        elem: '.demoMore'
        ,url:'/api/uploadimg'
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#logo").val(res.data);
        }
    })

});