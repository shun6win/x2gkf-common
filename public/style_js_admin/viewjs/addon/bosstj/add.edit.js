


var $, form;
var upload;

layui.config({
    base: '../../public/style_js_admin/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'upload', 'form'], function(){
    $ = layui.jquery
        ,form = layui.form;
    upload = layui.upload;
    var datatypeintarr = [];
   ($("input").each(function (index,e) {
       if($(e).attr("data-type") && $(e).attr("data-type")=="int"){
           datatypeintarr.push($(e).attr("name"))
       }
   }))
    $("#inputtypeints").val(datatypeintarr.join(","));
    form.val("layuiadmin-form-role",{"BossId":getQueryString('boss_id')});

    var id = getQueryString('id');
    if(id>0){
        layer.load(1)

        DianJinAjax("1",{dataCode: false,resCode: true},{
            url: './info',
            type: 'GET',
            data: {id: id},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值

                    form.val("layuiadmin-form-role",result.data);
                    $("#logoimg").attr("src",result.data.Logo);

                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }




});