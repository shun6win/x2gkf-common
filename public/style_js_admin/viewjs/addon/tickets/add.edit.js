
var vueapp = new Vue({
    el: '#vueapp',
    data: {
        btns: [],

    }, mounted: function () {

    },
    methods: {
        addbnt(){
            this.btns.push({
                title:"",
                url:"",
                webopen:1,
                appopen:1,
                wz:0,
            });
        }, delebnt(index){
            this.btns.splice(index,1);
        },submitvue(){
            $("#Btnjson").val(JSON.stringify(this.btns));
        }
    }
})

function deleVisitor(ismsg){


    layer.prompt({title: '确定清空请输入登入密码，并确定', formType: 1}, function(text, index){

        //执行 Ajax 后重载
        DianJinAjax(1,{dataCode: false,resCode: false}, {
            url: '/admin/visitor/del',
            data:{visitor_id:0,ismsg:ismsg,pass:text,boss_id:id},
            type: "get",
            success: function (res) {
                if(res.code==200){
                    setTimeout(function () {
                        layer.close(index);
                    },1000);
                }
                layer.alert(res.msg);
            }
        });


    });
}

var $, form;
var upload;
var id = getQueryString('id');
if(id>0){
    $("#deleVisitor").show();
}
layui.config({
    base: '../../public/style_js_admin/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'upload', 'form'], function(){
    $ = layui.jquery
        ,form = layui.form;
    upload = layui.upload;
    var datatypeintarr = [];
   ($("input").each(function (index,e) {
       if($(e).attr("data-type") && $(e).attr("data-type")=="int"){
           datatypeintarr.push($(e).attr("name"))
       }
   }))
    $("#inputtypeints").val(datatypeintarr.join(","));



    if(id>0){
        layer.load(1)

        DianJinAjax("1",{dataCode: false,resCode: true},{
            url: '/admin/boss/info',
            type: 'GET',
            data: {id: id},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值

                    form.val("layuiadmin-form-role",result.data);

                    if(result.data.Btnjson!=""){
                        vueapp.btns=JSON.parse(result.data.Btnjson);
                    }

                    $("#logoimg").attr("src",result.data.Logo);
                    $("#Adsimg").attr("src",result.data.Ads);
                    jQuery('#summernote').summernote('code', result.data.KefuGonggao);
                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }



    //同时绑定多个元素，并将属性设定在元素上
    upload.render({
        elem: '.demoMoref'
        ,url:'/api/uploadfile'
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#mp3").val(res.data);
        }
    })

    upload.render({
        elem: '.demoMore'
        ,url:'/api/uploadimg'
        ,data:{name:id,path:"config/images/bosslogo/"}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#logo").val(res.data);
            $("#logoimg").attr("src",res.data);

        }
    })
    upload.render({
        elem: '.demoMoreads'
        ,url:'/api/uploadimg'
        ,data:{name:"adslift"+id,path:"config/images/bosslogo/"}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#Ads").val(res.data);
            $("#Adsimg").attr("src",res.data);

        }
    })
});