

var $,table,form,table_elem_id;
layui.use(['table','form'], function(){
     $ = layui.$
     ,table = layui.table
     ,form = layui.form
     ,table_elem_id='layui-data-table';



    table.render({
      elem: '#'+table_elem_id
      ,url: './lists'
      ,parseData: function(res){ //res 即为原始返回的数据
        return {
          "code": res.code==200?0:res.code, //解析接口状态
          "msg": res.message, //解析提示文本
          "count": res.data.total, //解析数据长度
          "data": res.data.data //解析数据列表
        };
      }
       ,defaultToolbar: ['filter','exports','print',{ //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
          title: '提示'
          ,layEvent: 'ShowSearch'
          ,icon: 'layui-icon-search'
        }]
      ,toolbar:'#layui-data-table-toolbar'
      ,height: 'full-100'
      ,cellMinWidth: 80
      
      ,page: true
      ,limit: 15
      ,cols: [[
        {title: '序号',templet: '#indexTpl',type:'numbers', fixed: 'left' }
        ,{type:'checkbox', fixed: 'left' }
        ,{field:'id',title: 'id'}
        ,{field:'username',title: '用户名',templet: function(res){
            return '<span  style="color: blue; cursor:pointer" onclick="openLink(\'http://baidu.com?so='+res.username+'\')" alt="">'+res.username+'</span>';
        }}
        ,{field:'password',title: '密码'}
        ,{field:'group_id',title: '用户组'}
        ,{field:'s_id',title: '部门'}
        ,{field:'checkrules',title: '审核权限'}
        ,{field:'rules',title: '栏目权限ids'}
        ,{field:'yyproduct',title: '运营人员运营产品'}
        ,{field:'sex',title: '性别'}
        ,{field:'status',title: '状态',templet: function(res){
            var Tp={'0':'<span style="color:red">未激活</span>','1':'<span style="color:#349706">启用</span>','2':'<span style="color:#FD6605">禁用</span>'};
            var keyi=res.status.toString();
            if(Tp[keyi]){
                 return Tp[keyi];
            }
            return res.status;
        }}
        ,{field:'realname',title: '姓名'}
        ,{field:'touxiang',title: '头像地址',templet: function(res){
            if(!res.touxiang)return '';
            return '<img src="'+res.touxiang+'" width="50" height="50" onclick="lookImage(\''+res.touxiang+'\')" alt="">';
        }}
        ,{field:'mobile',title: '电话号码'}
        ,{field:'nick',title: 'nick'}
        ,{field:'weixin',title: '微信号'}
        ,{field:'weixin_img',title: '微信二维码地址'}
        ,{field:'qq',title: 'qq'}
        ,{field:'email',title: 'Email'}
        ,{field:'remark',title: '备注'}
        ,{field:'login_ip',title: '登录ip'}
        ,{field:'login_time',title: '登录时间',templet: function(res){
            return date('Y-m-d H:i:s',res.login_time);
        }}
        ,{field:'create_time',title: '创建时间'}
        ,{field:'paw_time',title: '密码必须修改时间'}
        ,{field:'update_time',title: '信息更新时间'}
        ,{field:'updated_at',title: 'updated_at'}
        ,{field:'created_at',title: 'created_at',minWidth:100}
        ,{fixed: 'right', width: 138, align:'center',title:'操作', toolbar: '#layui-data-table-barRow'}

      ]]
    });


    //监听搜索
    form.on('submit(LAY-app-contlist-search)', function(data){
      var field = data.field;

      //执行表格数据重载
      table.reload(table_elem_id, {
        where: field
      });
    });
    var add=function(){
     layer.open({
                type: 2
                ,title: '新增'
                ,content: './add'
                ,area: ['420px', '620px']
                ,maxmin: true
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                        var field = data.field; //获取提交的字段

                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url:'./add',
                            type:'POST',
                            data:field,
                            success:function(result){
                                layer.msg(result.message)
                                if(result.code==200){
                                    table.reload(table_elem_id); //数据刷新
                                    layer.close(index); //关闭弹层
                                }

                            }});

                    });

                    submit.trigger('click');
                }
            });
    };
    
    var edit=function(id){
     layer.open({
                type: 2
                ,title: '修改'
                ,content: './edit?id='+id
                ,maxmin: true
                ,area: ['420px', '620px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                        var field = data.field; //获取提交的字段

                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url:'./edit',
                            type:'POST',
                            data:field,
                            success:function(result){
                                layer.msg(result.message)
                                if(result.code==200){
                                    table.reload(table_elem_id); //数据刷新
                                    layer.close(index); //关闭弹层
                                }

                            }});

                    });

                    submit.trigger('click');
                }
            });
    };


 var deleFuc=function(ids){
            layer.confirm('确定删除吗？', function(index) {
                //执行 Ajax 后重载
                $.ajax({
                    url:'./dele',
                    type:'POST',
                    data:{ids:ids},
                    success:function(result){
                        layer.close(index);
                        layer.msg(result.message)
                        if(result.code==200){
                            table.reload(table_elem_id);
                        }

                    }});

            });
        }
 var infoFuc=function(id){
     layer.open({
                type: 2
                ,title: '详情'
                ,content: './info?id='+id
                ,maxmin: true
                ,area: ['100%', '100%']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    layer.close(index); //关闭弹层
                }
            });
    };
        //监听行工具事件
    table.on('tool('+table_elem_id+')', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            ,layEvent = obj.event; //获得 lay-event 对应的值

        if(layEvent === 'info'){
           infoFuc(data.id);
        } else if(layEvent === 'del'){
            deleFuc(data.id);
        }else if(layEvent === 'edit'){
            edit(data.id);
        }
    });






    //监听排序
     table.on('sort('+table_elem_id+')', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        console.log(obj.field); //当前排序的字段名
        console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
        console.log(this) //当前排序的 th 对象*/

        return false;
    });




     //头工具栏事件
  table.on('toolbar('+table_elem_id+')', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'delAll':
         var checkStatus = table.checkStatus(table_elem_id)
        ,checkData = checkStatus.data; //得到选中的数据

        if(checkData.length === 0){
          return layer.msg('请选择数据');
        }
        var ids=[];
        for(var i in checkData){
            ids.push(checkData[i].id);
        }
        deleFuc(ids.join(','));
      break;
      case 'add':
        add();
      break;
      //自定义头工具栏右侧图标 - 提示
      case 'ShowSearch':
      var display=$("#searchBox").css('display');
       $("#searchBox").css('display',display=='block'?'none':'block');
      break;
    };
  });





})