

var $, form;
layui.use(["form"], function () {
    $ = layui.$
        , form = layui.form;
    var id = getPar('id');
    if(id>0){
    layer.load(1)

        $.ajax({
            url: './info',
            type: 'GET',
            data: {id: id},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值
                    form.val("layuiadmin-form-role",result.data);
                    
                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }

    
})