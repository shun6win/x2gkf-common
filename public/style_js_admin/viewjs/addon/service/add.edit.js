var stpush = [];



function sfrom(boss_ids,fuc) {
    DianJinAjax("1",{dataCode: false,resCode: true},{
        url: '/admin/boss',
        type: 'POST',
        data: {limit: 120},
        success: function (result2) {
            for (var i in result2.data.data){
                var islk='';
                if(boss_ids.indexOf(result2.data.data[i].Id+"")!=-1){
                    islk='checked=""';
                }
                stpush.push(' <input '+islk+' type="checkbox" name="BossIds[]" value="'+result2.data.data[i].Id+'" title="'+result2.data.data[i].BossName+'">')
            }
            $("#BossIds").html(stpush.join(''))
            fuc();

        }}
    )
}


function setwarnurl(token) {

    var url=window.location.origin+'/api/sendwarn?sendtoken='+token+'&title=测试标题&content=测试内容替换你想要的&type=&clurl=&code='
    document.getElementById('warnurl').innerText=url;
    document.getElementById('warnurl').href=url;
}

var edittype = '';
if (typeof Addtype !== "undefined") {
    edittype=Addtype;
}
if (edittype=="editmy"){
    $(".edit-my").hide();
}else if (edittype=="editpass"){
    $(".edit-pass").hide();
}else if (edittype=="type3"){
    $(".type3").hide();
    $("#Groupid").val(Addkf);
    $("#ServiceType").val(3);

}

var $, form;
var upload;
var id = '';// getQueryString('id');
if (typeof ServiceId !== "undefined") {
    id=ServiceId;
}
layui.config({
    base: '../../public/style_js_admin/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'upload', 'form'], function(){
    $ = layui.jquery
        ,form = layui.form;
    upload = layui.upload;
    var datatypeintarr = $("#inputtypeints").val().split(",");
   ($("input").each(function (index,e) {
       if($(e).attr("data-type") && $(e).attr("data-type")=="int"){
           datatypeintarr.push($(e).attr("name"))
       }
   }))
    $("#inputtypeints").val(datatypeintarr.join(","));



    if(id>0){
        layer.load(1)
        $("#Password").attr("readonly","readonly");
        DianJinAjax("1",{dataCode: false,resCode: true},{
            url: './info',
            type: 'GET',
            data: {id: id},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值
                   /* for (d in result.data){
                        if (isNaN(result.data[d])){
                            datatypeintarr.push(d)
                        }
                    }
                     $("#inputtypeints").val(datatypeintarr.join(","));
                    */
                    result.data.BossIds= (result.data.BossIds+"").split(",")
                    $("#logoimg").attr("src",result.data.Avatar);
                    if(result.data.ServiceType==3){
                        $(".type3").hide();
                    }

                    sfrom(result.data.BossIds,function (){
                        form.val("layuiadmin-form-role",result.data);
                    })
                    setwarnurl(result.data.Sendtoken);

                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }else{
        sfrom([],function (){
            form.val("layuiadmin-form-role",[]);
        })
    }



    //同时绑定多个元素，并将属性设定在元素上
    upload.render({
        elem: '.demoMoref'
        ,url:'/api/uploadfile'

        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#mp3").val(res.data);
        }
    })

    upload.render({
        elem: '.demoMore'
        ,url:'/api/uploadimg'
        ,data:{name:id,path:"config/images/kfhead/"}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#logo").val(res.data);
            $("#logoimg").attr("src",res.data);
        }
    })

});