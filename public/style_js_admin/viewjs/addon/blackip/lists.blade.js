var boss_id=getQueryString('boss_id');

var $,table,form,table_elem_id;
layui.use(['table','form'], function(){
     $ = layui.$
     ,table = layui.table
     ,form = layui.form
     ,table_elem_id='layui-data-table';



    table.render({
      elem: '#'+table_elem_id
       ,method:'post'
      ,url: '?boss_id='+boss_id
      ,parseData: function(res){ //res 即为原始返回的数据
            res =DianJinAjaxSuccess(res);
        return {
          "code": res.code==200?0:res.code, //解析接口状态
          "msg": res.message, //解析提示文本
          "count": res.data.page.Total, //解析数据长度
          "data": res.data.data //解析数据列表
        };
      }
       ,defaultToolbar: ['filter','exports','print']
      ,toolbar:'#layui-data-table-toolbar'
      ,height: 'full-100'
      ,cellMinWidth: 80
      
      ,page: true
      ,limit: 15
      ,cols: [[
            {title: '序号',templet: '#indexTpl',type:'numbers', fixed: 'left' }
            ,{type:'checkbox', fixed: 'left' }
            //,{field:'Id',title: 'Id'}
            ,{field:'Ip',title: '黑名单ip'}
            ,{field:'Remark',title: '原因'}

           // ,{field:'BossId',title: 'BossId',minWidth:60}
            ,{field:'CreatedAt',title: '加入时间',width: 160}
            ,{fixed: 'right', width: 138, align:'center',title:'操作', toolbar: '#layui-data-table-barRow'}

      ]]
    });


    var submitFuc=function(index, layero){
        var iframeWindow = window['layui-layer-iframe'+ index]
            ,submitID = 'LAY-user-role-submit'
            ,submit = layero.find('iframe').contents().find('#'+ submitID)
            ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
            if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;
        //监听提交
        iframeWindow.layui.form.on('submit('+submitID+')', function(data){
            var field = data.field; //获取提交的字段
            var fieldint = (inputtypeints+"").split(",");
            console.log(field)
            console.log(fieldint)
            for (d in field){
                if (fieldint.indexOf(d)!=-1){
                    field[d]=field[d]-0;
                }
            }
            console.log(field)
            //提交 Ajax 成功后，静态更新表格中的数据
            DianJinAjax("1",{dataCode: true,resCode: false},{
                url:'./save',
                type:'POST',
                data:field,
                success:function(result){
                    layer.msg(result.msg)
                    if(result.code==200){
                        table.reload(table_elem_id); //数据刷新
                        layer.close(index); //关闭弹层
                    }

                }});

        });

        submit.trigger('click');
    }

    var add=function(){
     layer.open({
                type: 2
                ,title: '新增'
                ,content: './edit?boss_id='+boss_id
                ,area: ['420px', '620px']
                ,maxmin: true
                ,btn: ['确定', '取消']
                ,yes: submitFuc
            });
    };
    
    var edit=function(id){
     layer.open({
                type: 2
                ,title: '修改'
                ,content: './edit?id='+id
                ,maxmin: true
                ,area: ['420px', '620px']
                ,btn: ['确定', '取消']
                ,yes: submitFuc
            });
    };


 var deleFuc=function(ids){
            layer.confirm('确定删除吗？', function(index) {
                //执行 Ajax 后重载
                $.ajax({
                    url:'./dele',
                    type:'POST',
                    data:{ids:ids},
                    success:function(result){
                        layer.close(index);
                        layer.msg(result.msg)
                        if(result.code==200){
                            table.reload(table_elem_id);
                        }

                    }});

            });
        }
 var infoFuc=function(id){
     layer.open({
                type: 2
                ,title: '详情'
                ,content: './info?id='+id
                ,maxmin: true
                ,area: ['100%', '100%']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    layer.close(index); //关闭弹层
                }
            });
    };
        //监听行工具事件
    table.on('tool('+table_elem_id+')', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            ,layEvent = obj.event; //获得 lay-event 对应的值

        if(layEvent === 'info'){
           infoFuc(data.Id);
        } else if(layEvent === 'del'){
            deleFuc(data.Id);
        }else if(layEvent === 'edit'){
            edit(data.Id);
        }
    });






    //监听排序
     table.on('sort('+table_elem_id+')', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        console.log(obj.field); //当前排序的字段名
        console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
        console.log(this) //当前排序的 th 对象*/

        return false;
    });




     //头工具栏事件
  table.on('toolbar('+table_elem_id+')', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'delAll':
         var checkStatus = table.checkStatus(table_elem_id)
        ,checkData = checkStatus.data; //得到选中的数据

        if(checkData.length === 0){
          return layer.msg('请选择数据');
        }
        var ids=[];
        for(var i in checkData){
            ids.push(checkData[i].Id);
        }
        deleFuc(ids.join(','));
      break;
      case 'add':
        add();
      break;
      //自定义头工具栏右侧图标 - 提示
      case 'ShowSearch':
      var display=$("#searchBox").css('display');
       $("#searchBox").css('display',display=='block'?'none':'block');
      break;
    };
  });





})