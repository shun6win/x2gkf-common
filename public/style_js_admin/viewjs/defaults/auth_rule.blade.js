


var vuell= new Vue({
    el: "#demo",
    mounted () {
        this.get_data()
    },
    methods: {
        get_data(){
            var _this=this;
            $.ajax({
                url:"./lists_json",
                type:'POST',
                success:function(result){
                    _this.jsondata=result.data;
                }});
        },
        add(item){
            var _this=this;
            var postdata=_this.select_data;
            postdata.id=0;
            postdata.pid=item.pid;
            postdata.order=item.order;
            $.ajax({
                url:"./save",
                type:'POST',
                data:postdata,
                success:function(date){
                    layer.msg(date.message);
                    if(date.code==200){
                        _this.select_data = date.data;
                        _this.get_data();
                        setTimeout(function (){
                            window.location.href = "#" + date.data.id;
                        },100)
                    }

                }});
        },
        edit(item,key,value){
            var postdata = JSON.stringify(this.select_data);
             postdata = JSON.parse(postdata);
            var _this=this;
            $.ajax({
                url:"./save",
                type:'POST',
                data:key?{[key]:value,id:item.id}:postdata,
                success:function(date){
                    layer.msg(date.message);
                    if(date.code==200){
                        _this.select_data.id = 0;
                        _this.get_data();
                    }

                }});
        },
        dele(item){
            var _this = this;
            layer.confirm('确定删除 '+item.title+' 节点吗？', function(index) {

                //执行 Ajax 后重载
                $.ajax({
                    url:'./dele',
                    type:'POST',
                    data:{ids:item.id},
                    success:function(result){
                        layer.msg(result.message)
                        if(result.code==200){
                            _this.get_data();
                        }

                    }});

            });

        },
        select_icon(item){


            var _this=this;
            layer.open({
                type: 2 //此处以iframe举例
                ,title: '选择图标'
                ,area: ['80%', '90%']
                ,shade: 0
                ,maxmin: true
                ,content: './edit'
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'LAY-user-role-submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit(' + submitID + ')', function (data) {
                        var field = data.field; //获取提交的字段
                        field.id = item.id;
                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url: './save',
                            type: 'POST',
                            data: field,
                            success: function (result) {

                                layer.msg(result.message)
                                if (result.code == 200) {
                                    _this.get_data();

                                    layer.close(index); //关闭弹层
                                }

                            }
                        });

                    });

                    submit.trigger('click');
                }

            });




        },
        editbutton(item){
            var _this=this;
            layer.open({
                type: 2 //此处以iframe举例
                ,title: '编辑列表按钮菜单【 '+item.title+' 】'
                ,area: ['80%', '90%']
                ,shade: 0.3
                ,maxmin: true
                ,content: './editbutton?buttons='+encodeURI(item.buttons)
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'LAY-user-role-submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit(' + submitID + ')', function (data) {
                        var field = data.field; //获取提交的字段
                        field.id = item.id;
                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url: './save',
                            type: 'POST',
                            data: field,
                            success: function (result) {

                                layer.msg(result.message)
                                if (result.code == 200) {
                                    _this.select_data.id = 0;
                                    _this.get_data();

                                    layer.close(index); //关闭弹层
                                }

                            }
                        });

                    });

                    submit.trigger('click');
                }

            });


        },
        select_parent(item){


            var _this=this;
            layer.open({
                type: 2 //此处以iframe举例
                ,title: '选择【 '+item.title+' 】新父级'
                ,area: ['450px', '90%']
                ,shade: 0
                ,maxmin: true
                ,content: './lists?isselect=1'
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'LAY-user-role-submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit(' + submitID + ')', function (data) {
                        var field = data.field; //获取提交的字段
                        field.id = item.id;
                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url: './save',
                            type: 'POST',
                            data: field,
                            success: function (result) {

                                layer.msg(result.message)
                                if (result.code == 200) {
                                    _this.select_data.id = 0;
                                    _this.get_data();

                                    layer.close(index); //关闭弹层
                                }

                            }
                        });

                    });

                    submit.trigger('click');
                }

            });




        }
    },
    data() {
        return {
            sepid:-1,
            isselect:getPar('isselect'),
            select_data:{
                icon: ''
                ,id: ''
                ,index: ''
                ,order: ''
                ,ismenu: ''
                ,pid: ''
                ,rule: ''
                ,rule_group: ''
                ,title: ''
            },
            jsondata:[],
        }
    }
})


// 树组件
Vue.component('ew-tree', {
    template: `
        <ul class="l_tree">
            <li class="l_tree_branch" v-for="item in model" :key="item.id" :id="item.id" :class="(vuell.select_data.id==item.id && item.id)?'liselect':''">
                <div class="l_tree_click">
                    <i :class="item.icon" class="layui-icon"></i>
                    <button type="button" class="l_tree_children_btn" v-if="item.children.length>0"  @click="toggle(item)">{{ !item.show ? '-' : '+' }}</button>



<template v-if="vuell.isselect">
 <span  class="l_folder"  @click="selsct_click_rio(item)"  >
         {{ item.title }}

         <template v-if="vuell.sepid==item.id">
         <i style="margin-left: 20px;font-weight: bold;font-size: 22px" class="layui-icon layui-icon-ok"></i>
         </template>

  </span>

</template>
<template v-else>
                    <span  v-if="vuell.select_data.id==item.id && item.id"  class="l_folder l_folder_sele" >
                           <input type="text" name="title"  v-model="vuell.select_data.title"  style="width: 200px;display: inline;" class="layui-input">
                            <span style="width: 800px;float: right;color: #0C0C0C">
                                 <table ><tr>
                                     <td class="td1"><input v-if="item.children.length==0" type="text" name="rule"  v-model="vuell.select_data.rule"   placeholder="如：/index/index/list"  class="layui-input"></td>
                                     <td class="td2"><input type="number"   v-model="vuell.select_data.order"    class="layui-input"></td>
                                     <td class="td3"><i :class="item.icon" class="layui-icon"></i>
                                         <button type="button" v-if="!item.icon" @click="vuell.select_icon(item)" class="layui-btn layui-btn-primary layui-btn-sm">
                                            icon
                                          </button>
                                      </td>
                                     <td class="td4" style="padding-left: 12px">
                                     <div @click="vuell.edit(item,'ismenu',item.ismenu?0:1)"  class="layui-unselect layui-form-switch" :class="item.ismenu?'layui-form-onswitch':''">
                                     <em>{{ ['否','是'][(item.ismenu)] }}</em><i></i></div></td>

                                        </td>
                                      <td style="padding-left: 12px">
                                        <div class="layui-btn-group">
                                          <button type="button" @click="vuell.edit(vuell.select_data)" class="layui-btn layui-btn-primary layui-btn-sm">
                                            保存
                                          </button>
                                          <button type="button" @click="vuell.select_parent(item)" class="layui-btn layui-btn-primary layui-btn-sm">
                                            调整位置
                                          </button>
                                          <button type="button" @click="vuell.select_data.id=0" class="layui-btn layui-btn-primary layui-btn-sm">
                                            取消
                                          </button>
                                        </div>

                                        </td>
                                    </tr>
                                </table>

                              </span>
                    </span>




 <span v-else-if="item.id"  class="l_folder"   >
                    {{ item.title }}
                            <span style="width: 800px;float: right;color: #0C0C0C">

                             <table ><tr>
                             <td class="td1">{{ item.children.length==0?((item.rule+'').length>40?item.rule.substring(0,40)+'…':item.rule):'' }}</td>
                             <td class="td2">{{ item.order }}</td>
                             <td class="td3" @click="vuell.select_icon(item)"><i :class="item.icon" class="layui-icon"></i></td>
                             <td class="td4">
                             <div @click="vuell.edit(item,'ismenu',item.ismenu?0:1)"  class="layui-unselect layui-form-switch" :class="item.ismenu?'layui-form-onswitch':''"><em>{{ ['否','是'][item.ismenu] }}</em><i></i></div></td>

                              <td>
                                    <div class="layui-btn-group">
                                       <button type="button" class="layui-btn layui-btn-primary layui-btn-sm" @click="select_data_run(item)">
                                        <i class="layui-icon">&#xe642;</i>
                                      </button>
                                      <button type="button" @click="vuell.add(item)" class="layui-btn layui-btn-primary layui-btn-sm">
                                        <i class="layui-icon">&#xe654;</i>
                                      </button>

                                      <button type="button" @click="vuell.dele(item)" class="layui-btn layui-btn-primary layui-btn-sm">
                                        <i class="layui-icon">&#xe640;</i>
                                      </button>
                                      <button type="button" @click="vuell.editbutton(item)" :class="item.buttons.length<5?'layui-btn-primary':''" class="layui-btn  layui-btn-sm">
                                        子
                                      </button>
                                    </div>


                                    </td>
                            </tr>
                            </table>
                              </span>
                    </span>





     <span v-else  class="l_folder" style="color: #0C0C0C;font-weight: bold" >
                        {{ item.title }}
                        <span style="width: 800px;float: right;">

                         <table ><tr>
                         <td class="td1">规则</td>
                         <td class="td2">排序</td>
                         <td class="td3">图标</td>
                         <td class="td4">菜单</td>
                          <td>

                            <button type="button" @click="vuell.add(item)" class="layui-btn layui-btn-primary layui-btn-sm">
                                        <i class="layui-icon">&#xe654;</i>
                                      </button>

                        </td>
                        </tr>
                        </table>
                          </span>
                        </span>

</template>




                </div>
                <ew-tree v-show="!item.show" v-if="item.children" :model="item.children"></ew-tree>
            </li>
        </ul>`,
    props: {
        model: {
        }
    },
    methods: {
        toggle: function (item) {
             var idx = this.model.indexOf(item)
             Vue.set(this.model[idx], 'show', !item.show)
        },
    selsct_click_rio(item){
            vuell.sepid=item.id;
        $("#pid").val(item.id);
    }
    ,
        select_data_run(datass){
            vuell.select_data={
                icon: datass.icon
                ,id: datass.id
                ,index: datass.index
                ,order: datass.order
                ,ismenu: datass.ismenu
                ,pid:datass.pid
                ,rule: datass.rule
                ,rule_group: datass.rule_group
                ,title: datass.title
            };
        }
    }
});


var $;
layui.use(['form'], function(){
     $ = layui.$
        ,form = layui.form ;

})
