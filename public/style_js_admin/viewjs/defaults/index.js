var x2O=getCookie("x2O");
var x2V=getCookie("x2V");
var vueapp = new Vue({
    el: '#vueapp',
    data: {
        btns: [],
        Vmenus: [
            {
                "title":"商家管理",
                "icon":"layui-icon-group",
                "url":"/admin/boss?11",
                "isshow":true
            },{
                "title":"客服管理",
                "icon":"layui-icon-chat",
                "url":"/admin/service/",
                "isshow":true
            },{
                "title":"工单管理",
                "icon":"layui-icon-chat",
                "url":"/admin/tickets/list",
                "isshow":viptf("tickets")
            },{
                "title":"服务器编号设置",
                "icon":"layui-icon-link",
                "url":"/admin/appset",
                "isshow":true
            },{
                "title":"站点设置",
                "icon":"layui-icon-set",
                "url":"",
                "isshow":true,
                "menus":[
                    {
                        "title":"翻译接口设置",
                        "icon":"layui-icon-util",
                        "url":"/admin/fanyiset",
                        "isshow":viptf("fy")
                    },{
                        "title":"域名黑白名单",
                        "icon":"layui-icon-vercode",
                        "url":"/admin/domainwhite",
                        "isshow":true
                    },{
                        "title":"站点信息配置",
                        "icon":"layui-icon-set",
                        "url":"/admin/webinfoset",
                        "isshow":true
                    },{
                        "title":"email配置",
                        "icon":"layui-icon-link",
                        "url":"/admin/config/email",
                        "isshow":true
                    },{
                        "title":"APP版本更新信息",
                        "icon":"layui-icon-android",
                        "url":"/admin/verupdateapp",
                        "isshow":true
                    },{
                        "title":"信息清空",
                        "icon":"layui-icon-delete",
                        "url":"/admin/clear",
                        "isshow":true
                    },{
                        "title":"注册配置",
                        "icon":"layui-icon-link",
                        "url":"/admin/config/reg",
                        "isshow":viptf("reg")
                    }
                ]
            }
        ],
    }, mounted: function () {
        setCookie("isMin",window.innerWidth<500?'1':'')
    },
    methods: {
        addbnt(){
            this.btns.push({
                title:"",
                url:"",
                webopen:1,
                appopen:1,
            });
        }, delebnt(index){
            this.btns.splice(index,1);
        },submitvue(){
            $("#Btnjson").val(JSON.stringify(this.btns));
        }
    }
})