function logout() {
    layer.confirm('确定退出吗？', function(index) {
        //执行 Ajax 后重载
        DianJinAjax(1,{dataCode: true,resCode: true}, {
            url: '/service/logout',
            type: "get",
            success: function (res) {
                window.location.reload();
            }
        });


    });
}
var editMyIsRead=false;
function editMy(id,isread=false) {
    editMyIsRead=isread;
    layer.open({
        type: 2
        ,title: '修改我的基本资料'
        ,content: '/admin/visitor/edit/my'
        ,maxmin: true
        ,area: ['420px', '620px']
        ,btn: ['确定', '取消']
        ,yes:  submitMyFuc
    });
}

function editMyPass() {
    layer.open({
        type: 2
        ,title: '修改我的密码'
        ,content: '/admin/service/editpass'
        ,maxmin: true
        ,area: ['420px', '310px']
        ,btn: ['确定', '取消']
        ,yes: submitMyFucp
    });
}

var submitMyFucp=function(index, layero){
    var iframeWindow = window['layui-layer-iframe'+ index]
        ,submitID = 'LAY-user-role-submit'
        ,submit = layero.find('iframe').contents().find('#'+ submitID)
        ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
    if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;
    //监听提交
    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
        var field = data.field; //获取提交的字段
        var fieldint = (inputtypeints+"").split(",");

        //提交 Ajax 成功后，静态更新表格中的数据
        DianJinAjax("1",{dataCode: true,resCode: false},{
            url:'/admin/service/editpass',
            type:'POST',
            data:field,
            success:function(result){
                layer.alert(result.msg);
                if(result.code==200){
                    setTimeout(function () {
                        window.location.href="/service/login";
                    },2000)
                }

            }});

    });

    submit.trigger('click');
}


var submitMyFuc=function(index, layero){
    var iframeWindow = window['layui-layer-iframe'+ index]
        ,submitID = 'LAY-user-role-submit'
        ,submit = layero.find('iframe').contents().find('#'+ submitID)
        ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
    if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;
    //监听提交
    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
        var field = data.field; //获取提交的字段
        //提交 Ajax 成功后，静态更新表格中的数据
        DianJinAjax("1",{dataCode: true,resCode: false},{
            url:'/admin/visitor/edit/my',
            type:'POST',
            data:field,
            success:function(result){
                layer.msg(result.msg)
                if(result.code==200){
                    setTimeout(function () {
                        window.location.reload();
                    },3000)
                }


            }});

    });

    submit.trigger('click');
}

