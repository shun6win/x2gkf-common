
var o03ewOO0037whsjsa=o03ewOO0037whsjsa||(function(Math,undefined){var C={};var C_lib=C.lib={};var Base=C_lib.Base=(function(){function F(){}return{extend:function(overrides){F.prototype=this;var subtype=new F();if(overrides){subtype.mixIn(overrides)}if(!subtype.hasOwnProperty("init")){subtype.init=function(){subtype.$super.init.apply(this,arguments)}}subtype.init.prototype=subtype;subtype.$super=this;return subtype},create:function(){var instance=this.extend();instance.init.apply(instance,arguments);return instance},init:function(){},mixIn:function(properties){for(var propertyName in properties){if(properties.hasOwnProperty(propertyName)){this[propertyName]=properties[propertyName]}}if(properties.hasOwnProperty("toString")){this.toString=properties.toString}},clone:function(){return this.init.prototype.extend(this)}}}());var WordArray=C_lib.WordArray=Base.extend({init:function(words,sigBytes){words=this.words=words||[];if(sigBytes!=undefined){this.sigBytes=sigBytes}else{this.sigBytes=words.length*4}},toString:function(encoder){return(encoder||Hex).stringify(this)},concat:function(wordArray){var thisWords=this.words;var thatWords=wordArray.words;var thisSigBytes=this.sigBytes;var thatSigBytes=wordArray.sigBytes;this.clamp();if(thisSigBytes%4){for(var i=0;i<thatSigBytes;i++){var thatByte=(thatWords[i>>>2]>>>(24-(i%4)*8))&255;thisWords[(thisSigBytes+i)>>>2]|=thatByte<<(24-((thisSigBytes+i)%4)*8)}}else{if(thatWords.length>65535){for(var i=0;i<thatSigBytes;i+=4){thisWords[(thisSigBytes+i)>>>2]=thatWords[i>>>2]}}else{thisWords.push.apply(thisWords,thatWords)}}this.sigBytes+=thatSigBytes;return this},clamp:function(){var words=this.words;var sigBytes=this.sigBytes;words[sigBytes>>>2]&=4294967295<<(32-(sigBytes%4)*8);words.length=Math.ceil(sigBytes/4)},clone:function(){var clone=Base.clone.call(this);clone.words=this.words.slice(0);return clone},random:function(nBytes){var words=[];for(var i=0;i<nBytes;i+=4){words.push((Math.random()*4294967296)|0)}return new WordArray.init(words,nBytes)}});var C_enc=C.enc={};var Hex=C_enc.Hex={stringify:function(wordArray){var words=wordArray.words;var sigBytes=wordArray.sigBytes;var hexChars=[];for(var i=0;i<sigBytes;i++){var bite=(words[i>>>2]>>>(24-(i%4)*8))&255;hexChars.push((bite>>>4).toString(16));hexChars.push((bite&15).toString(16))}return hexChars.join("")},parse:function(hexStr){var hexStrLength=hexStr.length;var words=[];for(var i=0;i<hexStrLength;i+=2){words[i>>>3]|=parseInt(hexStr.substr(i,2),16)<<(24-(i%8)*4)}return new WordArray.init(words,hexStrLength/2)}};var Latin1=C_enc.Latin1={stringify:function(wordArray){var words=wordArray.words;var sigBytes=wordArray.sigBytes;var latin1Chars=[];for(var i=0;i<sigBytes;i++){var bite=(words[i>>>2]>>>(24-(i%4)*8))&255;latin1Chars.push(String.fromCharCode(bite))}return latin1Chars.join("")},parse:function(latin1Str){var latin1StrLength=latin1Str.length;var words=[];for(var i=0;i<latin1StrLength;i++){words[i>>>2]|=(latin1Str.charCodeAt(i)&255)<<(24-(i%4)*8)}return new WordArray.init(words,latin1StrLength)}};var Utf8=C_enc.Utf8={stringify:function(wordArray){try{return decodeURIComponent(escape(Latin1.stringify(wordArray)))}catch(e){throw new Error("Malformed UTF-8 data")}},parse:function(utf8Str){return Latin1.parse(unescape(encodeURIComponent(utf8Str)))}};var BufferedBlockAlgorithm=C_lib.BufferedBlockAlgorithm=Base.extend({reset:function(){this._data=new WordArray.init();this._nDataBytes=0},_append:function(data){if(typeof data=="string"){data=Utf8.parse(data)}this._data.concat(data);this._nDataBytes+=data.sigBytes},_process:function(doFlush){var data=this._data;var dataWords=data.words;var dataSigBytes=data.sigBytes;var blockSize=this.blockSize;var blockSizeBytes=blockSize*4;var nBlocksReady=dataSigBytes/blockSizeBytes;if(doFlush){nBlocksReady=Math.ceil(nBlocksReady)}else{nBlocksReady=Math.max((nBlocksReady|0)-this._minBufferSize,0)}var nWordsReady=nBlocksReady*blockSize;var nBytesReady=Math.min(nWordsReady*4,dataSigBytes);if(nWordsReady){for(var offset=0;offset<nWordsReady;offset+=blockSize){this._doProcessBlock(dataWords,offset)}var processedWords=dataWords.splice(0,nWordsReady);data.sigBytes-=nBytesReady}return new WordArray.init(processedWords,nBytesReady)},clone:function(){var clone=Base.clone.call(this);clone._data=this._data.clone();return clone},_minBufferSize:0});var Hasher=C_lib.Hasher=BufferedBlockAlgorithm.extend({cfg:Base.extend(),init:function(cfg){this.cfg=this.cfg.extend(cfg);this.reset()},reset:function(){BufferedBlockAlgorithm.reset.call(this);this._doReset()},update:function(messageUpdate){this._append(messageUpdate);this._process();return this},finalize:function(messageUpdate){if(messageUpdate){this._append(messageUpdate)}var hash=this._doFinalize();return hash},blockSize:512/32,_createHelper:function(hasher){return function(message,cfg){return new hasher.init(cfg).finalize(message)}},_createHmacHelper:function(hasher){return function(message,key){return new C_algo.HMAC.init(hasher,key).finalize(message)}}});var C_algo=C.algo={};
    return C}(Math));(function(){var C=o03ewOO0037whsjsa;var C_lib=C.lib;var WordArray=C_lib.WordArray;var C_enc=C.enc;var Base64=C_enc.Base64={stringify:function(wordArray){var words=wordArray.words;var sigBytes=wordArray.sigBytes;var map=this._map;wordArray.clamp();var base64Chars=[];for(var i=0;i<sigBytes;i+=3){var byte1=(words[i>>>2]>>>(24-(i%4)*8))&255;var byte2=(words[(i+1)>>>2]>>>(24-((i+1)%4)*8))&255;var byte3=(words[(i+2)>>>2]>>>(24-((i+2)%4)*8))&255;var triplet=(byte1<<16)|(byte2<<8)|byte3;for(var j=0;(j<4)&&(i+j*0.75<sigBytes);j++){base64Chars.push(map.charAt((triplet>>>(6*(3-j)))&63))}}var paddingChar=map.charAt(64);if(paddingChar){while(base64Chars.length%4){base64Chars.push(paddingChar)}}return base64Chars.join("")},parse:function(base64Str){var base64StrLength=base64Str.length;var map=this._map;var paddingChar=map.charAt(64);if(paddingChar){var paddingIndex=base64Str.indexOf(paddingChar);if(paddingIndex!=-1){base64StrLength=paddingIndex}}var words=[];var nBytes=0;for(var i=0;i<base64StrLength;i++){if(i%4){var bits1=map.indexOf(base64Str.charAt(i-1))<<((i%4)*2);var bits2=map.indexOf(base64Str.charAt(i))>>>(6-(i%4)*2);words[nBytes>>>2]|=(bits1|bits2)<<(24-(nBytes%4)*8);nBytes++}}return WordArray.create(words,nBytes)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}}());o03ewOO0037whsjsa.lib.Cipher||(function(undefined){var C=o03ewOO0037whsjsa;var C_lib=C.lib;var Base=C_lib.Base;var WordArray=C_lib.WordArray;var BufferedBlockAlgorithm=C_lib.BufferedBlockAlgorithm;var C_enc=C.enc;var Utf8=C_enc.Utf8;var Base64=C_enc.Base64;var C_algo=C.algo;var EvpKDF=C_algo.EvpKDF;var Cipher=C_lib.Cipher=BufferedBlockAlgorithm.extend({cfg:Base.extend(),createEncryptor:function(key,cfg){return this.create(this._ENC_XFORM_MODE,key,cfg)},createDecryptor:function(key,cfg){return this.create(this._DEC_XFORM_MODE,key,cfg)},init:function(xformMode,key,cfg){this.cfg=this.cfg.extend(cfg);this._xformMode=xformMode;this._key=key;this.reset()},reset:function(){BufferedBlockAlgorithm.reset.call(this);this._doReset()},process:function(dataUpdate){this._append(dataUpdate);return this._process()},finalize:function(dataUpdate){if(dataUpdate){this._append(dataUpdate)}var finalProcessedData=this._doFinalize();return finalProcessedData},keySize:128/32,ivSize:128/32,_ENC_XFORM_MODE:1,_DEC_XFORM_MODE:2,_createHelper:(function(){function selectCipherStrategy(key){if(typeof key=="string"){return PasswordBasedCipher}else{return SerializableCipher}}return function(cipher){return{encrypt:function(message,key,cfg){return selectCipherStrategy(key).encrypt(cipher,message,key,cfg)},decrypt:function(ciphertext,key,cfg){return selectCipherStrategy(key).decrypt(cipher,ciphertext,key,cfg)}}}}())});var StreamCipher=C_lib.StreamCipher=Cipher.extend({_doFinalize:function(){var finalProcessedBlocks=this._process(!!"flush");return finalProcessedBlocks},blockSize:1});var C_mode=C.mode={};var BlockCipherMode=C_lib.BlockCipherMode=Base.extend({createEncryptor:function(cipher,iv){return this.Encryptor.create(cipher,iv)},createDecryptor:function(cipher,iv){return this.Decryptor.create(cipher,iv)},init:function(cipher,iv){this._cipher=cipher;this._iv=iv}});var CBC=C_mode.CBC=(function(){var CBC=BlockCipherMode.extend();CBC.Encryptor=CBC.extend({processBlock:function(words,offset){var cipher=this._cipher;var blockSize=cipher.blockSize;xorBlock.call(this,words,offset,blockSize);cipher.encryptBlock(words,offset);this._prevBlock=words.slice(offset,offset+blockSize)}});CBC.Decryptor=CBC.extend({processBlock:function(words,offset){var cipher=this._cipher;var blockSize=cipher.blockSize;var thisBlock=words.slice(offset,offset+blockSize);cipher.decryptBlock(words,offset);xorBlock.call(this,words,offset,blockSize);this._prevBlock=thisBlock}});function xorBlock(words,offset,blockSize){var iv=this._iv;if(iv){var block=iv;this._iv=undefined}else{var block=this._prevBlock}for(var i=0;i<blockSize;i++){words[offset+i]^=block[i]}}return CBC}());var C_pad=C.pad={};var Pkcs7=C_pad.Pkcs7={pad:function(data,blockSize){var blockSizeBytes=blockSize*4;var nPaddingBytes=blockSizeBytes-data.sigBytes%blockSizeBytes;var paddingWord=(nPaddingBytes<<24)|(nPaddingBytes<<16)|(nPaddingBytes<<8)|nPaddingBytes;var paddingWords=[];for(var i=0;i<nPaddingBytes;i+=4){paddingWords.push(paddingWord)}var padding=WordArray.create(paddingWords,nPaddingBytes);data.concat(padding)},unpad:function(data){var nPaddingBytes=data.words[(data.sigBytes-1)>>>2]&255;data.sigBytes-=nPaddingBytes}};var BlockCipher=C_lib.BlockCipher=Cipher.extend({cfg:Cipher.cfg.extend({mode:CBC,padding:Pkcs7}),reset:function(){Cipher.reset.call(this);var cfg=this.cfg;var iv=cfg.iv;var mode=cfg.mode;if(this._xformMode==this._ENC_XFORM_MODE){var modeCreator=mode.createEncryptor}else{var modeCreator=mode.createDecryptor;this._minBufferSize=1}this._mode=modeCreator.call(mode,this,iv&&iv.words)},_doProcessBlock:function(words,offset){this._mode.processBlock(words,offset)},_doFinalize:function(){var padding=this.cfg.padding;if(this._xformMode==this._ENC_XFORM_MODE){padding.pad(this._data,this.blockSize);
        var finalProcessedBlocks=this._process(!!"flush")}else{var finalProcessedBlocks=this._process(!!"flush");padding.unpad(finalProcessedBlocks)}return finalProcessedBlocks},blockSize:128/32});var CipherParams=C_lib.CipherParams=Base.extend({init:function(cipherParams){this.mixIn(cipherParams)},toString:function(formatter){return(formatter||this.formatter).stringify(this)}});var C_format=C.format={};var OpenSSLFormatter=C_format.OpenSSL={stringify:function(cipherParams){var ciphertext=cipherParams.ciphertext;var salt=cipherParams.salt;if(salt){var wordArray=WordArray.create([1398893684,1701076831]).concat(salt).concat(ciphertext)}else{var wordArray=ciphertext}return wordArray.toString(Base64)},parse:function(openSSLStr){var ciphertext=Base64.parse(openSSLStr);var ciphertextWords=ciphertext.words;if(ciphertextWords[0]==1398893684&&ciphertextWords[1]==1701076831){var salt=WordArray.create(ciphertextWords.slice(2,4));ciphertextWords.splice(0,4);ciphertext.sigBytes-=16}return CipherParams.create({ciphertext:ciphertext,salt:salt})}};var SerializableCipher=C_lib.SerializableCipher=Base.extend({cfg:Base.extend({format:OpenSSLFormatter}),encrypt:function(cipher,message,key,cfg){cfg=this.cfg.extend(cfg);var encryptor=cipher.createEncryptor(key,cfg);var ciphertext=encryptor.finalize(message);var cipherCfg=encryptor.cfg;return CipherParams.create({ciphertext:ciphertext,key:key,iv:cipherCfg.iv,algorithm:cipher,mode:cipherCfg.mode,padding:cipherCfg.padding,blockSize:cipher.blockSize,formatter:cfg.format})},decrypt:function(cipher,ciphertext,key,cfg){cfg=this.cfg.extend(cfg);ciphertext=this._parse(ciphertext,cfg.format);var plaintext=cipher.createDecryptor(key,cfg).finalize(ciphertext.ciphertext);return plaintext},_parse:function(ciphertext,format){if(typeof ciphertext=="string"){return format.parse(ciphertext,this)}else{return ciphertext}}});var C_kdf=C.kdf={};var OpenSSLKdf=C_kdf.OpenSSL={execute:function(password,keySize,ivSize,salt){if(!salt){salt=WordArray.random(64/8)}var key=EvpKDF.create({keySize:keySize+ivSize}).compute(password,salt);var iv=WordArray.create(key.words.slice(keySize),ivSize*4);key.sigBytes=keySize*4;return CipherParams.create({key:key,iv:iv,salt:salt})}};var PasswordBasedCipher=C_lib.PasswordBasedCipher=SerializableCipher.extend({cfg:SerializableCipher.cfg.extend({kdf:OpenSSLKdf}),encrypt:function(cipher,message,password,cfg){cfg=this.cfg.extend(cfg);var derivedParams=cfg.kdf.execute(password,cipher.keySize,cipher.ivSize);cfg.iv=derivedParams.iv;var ciphertext=SerializableCipher.encrypt.call(this,cipher,message,derivedParams.key,cfg);ciphertext.mixIn(derivedParams);return ciphertext},decrypt:function(cipher,ciphertext,password,cfg){cfg=this.cfg.extend(cfg);ciphertext=this._parse(ciphertext,cfg.format);var derivedParams=cfg.kdf.execute(password,cipher.keySize,cipher.ivSize,ciphertext.salt);cfg.iv=derivedParams.iv;var plaintext=SerializableCipher.decrypt.call(this,cipher,ciphertext,derivedParams.key,cfg);return plaintext}})}());(function(){var C=o03ewOO0037whsjsa;var C_lib=C.lib;var BlockCipher=C_lib.BlockCipher;var C_algo=C.algo;var SBOX=[];var INV_SBOX=[];var SUB_MIX_0=[];var SUB_MIX_1=[];var SUB_MIX_2=[];var SUB_MIX_3=[];var INV_SUB_MIX_0=[];var INV_SUB_MIX_1=[];var INV_SUB_MIX_2=[];var INV_SUB_MIX_3=[];(function(){var d=[];for(var i=0;i<256;i++){if(i<128){d[i]=i<<1}else{d[i]=(i<<1)^283}}var x=0;var xi=0;for(var i=0;i<256;i++){var sx=xi^(xi<<1)^(xi<<2)^(xi<<3)^(xi<<4);sx=(sx>>>8)^(sx&255)^99;SBOX[x]=sx;INV_SBOX[sx]=x;var x2=d[x];var x4=d[x2];var x8=d[x4];var t=(d[sx]*257)^(sx*16843008);SUB_MIX_0[x]=(t<<24)|(t>>>8);SUB_MIX_1[x]=(t<<16)|(t>>>16);SUB_MIX_2[x]=(t<<8)|(t>>>24);SUB_MIX_3[x]=t;var t=(x8*16843009)^(x4*65537)^(x2*257)^(x*16843008);INV_SUB_MIX_0[sx]=(t<<24)|(t>>>8);INV_SUB_MIX_1[sx]=(t<<16)|(t>>>16);INV_SUB_MIX_2[sx]=(t<<8)|(t>>>24);INV_SUB_MIX_3[sx]=t;if(!x){x=xi=1}else{x=x2^d[d[d[x8^x2]]];xi^=d[d[xi]]}}}());var RCON=[0,1,2,4,8,16,32,64,128,27,54];var AES=C_algo.AES=BlockCipher.extend({_doReset:function(){var key=this._key;var keyWords=key.words;var keySize=key.sigBytes/4;var nRounds=this._nRounds=keySize+6;var ksRows=(nRounds+1)*4;var keySchedule=this._keySchedule=[];for(var ksRow=0;ksRow<ksRows;ksRow++){if(ksRow<keySize){keySchedule[ksRow]=keyWords[ksRow]}else{var t=keySchedule[ksRow-1];if(!(ksRow%keySize)){t=(t<<8)|(t>>>24);t=(SBOX[t>>>24]<<24)|(SBOX[(t>>>16)&255]<<16)|(SBOX[(t>>>8)&255]<<8)|SBOX[t&255];t^=RCON[(ksRow/keySize)|0]<<24}else{if(keySize>6&&ksRow%keySize==4){t=(SBOX[t>>>24]<<24)|(SBOX[(t>>>16)&255]<<16)|(SBOX[(t>>>8)&255]<<8)|SBOX[t&255]}}keySchedule[ksRow]=keySchedule[ksRow-keySize]^t}}var invKeySchedule=this._invKeySchedule=[];for(var invKsRow=0;invKsRow<ksRows;invKsRow++){var ksRow=ksRows-invKsRow;if(invKsRow%4){var t=keySchedule[ksRow]}else{var t=keySchedule[ksRow-4]}if(invKsRow<4||ksRow<=4){invKeySchedule[invKsRow]=t}else{invKeySchedule[invKsRow]=INV_SUB_MIX_0[SBOX[t>>>24]]^INV_SUB_MIX_1[SBOX[(t>>>16)&255]]^INV_SUB_MIX_2[SBOX[(t>>>8)&255]]^INV_SUB_MIX_3[SBOX[t&255]]}}},encryptBlock:function(M,offset){this._doCryptBlock(M,offset,this._keySchedule,SUB_MIX_0,SUB_MIX_1,SUB_MIX_2,SUB_MIX_3,SBOX)
    },decryptBlock:function(M,offset){var t=M[offset+1];M[offset+1]=M[offset+3];M[offset+3]=t;this._doCryptBlock(M,offset,this._invKeySchedule,INV_SUB_MIX_0,INV_SUB_MIX_1,INV_SUB_MIX_2,INV_SUB_MIX_3,INV_SBOX);var t=M[offset+1];M[offset+1]=M[offset+3];M[offset+3]=t},_doCryptBlock:function(M,offset,keySchedule,SUB_MIX_0,SUB_MIX_1,SUB_MIX_2,SUB_MIX_3,SBOX){var nRounds=this._nRounds;var s0=M[offset]^keySchedule[0];var s1=M[offset+1]^keySchedule[1];var s2=M[offset+2]^keySchedule[2];var s3=M[offset+3]^keySchedule[3];var ksRow=4;for(var round=1;round<nRounds;round++){var t0=SUB_MIX_0[s0>>>24]^SUB_MIX_1[(s1>>>16)&255]^SUB_MIX_2[(s2>>>8)&255]^SUB_MIX_3[s3&255]^keySchedule[ksRow++];var t1=SUB_MIX_0[s1>>>24]^SUB_MIX_1[(s2>>>16)&255]^SUB_MIX_2[(s3>>>8)&255]^SUB_MIX_3[s0&255]^keySchedule[ksRow++];var t2=SUB_MIX_0[s2>>>24]^SUB_MIX_1[(s3>>>16)&255]^SUB_MIX_2[(s0>>>8)&255]^SUB_MIX_3[s1&255]^keySchedule[ksRow++];var t3=SUB_MIX_0[s3>>>24]^SUB_MIX_1[(s0>>>16)&255]^SUB_MIX_2[(s1>>>8)&255]^SUB_MIX_3[s2&255]^keySchedule[ksRow++];s0=t0;s1=t1;s2=t2;s3=t3}var t0=((SBOX[s0>>>24]<<24)|(SBOX[(s1>>>16)&255]<<16)|(SBOX[(s2>>>8)&255]<<8)|SBOX[s3&255])^keySchedule[ksRow++];var t1=((SBOX[s1>>>24]<<24)|(SBOX[(s2>>>16)&255]<<16)|(SBOX[(s3>>>8)&255]<<8)|SBOX[s0&255])^keySchedule[ksRow++];var t2=((SBOX[s2>>>24]<<24)|(SBOX[(s3>>>16)&255]<<16)|(SBOX[(s0>>>8)&255]<<8)|SBOX[s1&255])^keySchedule[ksRow++];var t3=((SBOX[s3>>>24]<<24)|(SBOX[(s0>>>16)&255]<<16)|(SBOX[(s1>>>8)&255]<<8)|SBOX[s2&255])^keySchedule[ksRow++];M[offset]=t0;M[offset+1]=t1;M[offset+2]=t2;M[offset+3]=t3},keySize:256/32});C.AES=BlockCipher._createHelper(AES)}());(function(Math){var C=o03ewOO0037whsjsa;var C_lib=C.lib;var WordArray=C_lib.WordArray;var Hasher=C_lib.Hasher;var C_algo=C.algo;var T=[];(function(){for(var i=0;i<64;i++){T[i]=(Math.abs(Math.sin(i+1))*4294967296)|0}}());var MD5=C_algo.MD5=Hasher.extend({_doReset:function(){this._hash=new WordArray.init([1732584193,4023233417,2562383102,271733878])},_doProcessBlock:function(M,offset){for(var i=0;i<16;i++){var offset_i=offset+i;var M_offset_i=M[offset_i];M[offset_i]=((((M_offset_i<<8)|(M_offset_i>>>24))&16711935)|(((M_offset_i<<24)|(M_offset_i>>>8))&4278255360))}var H=this._hash.words;var M_offset_0=M[offset+0];var M_offset_1=M[offset+1];var M_offset_2=M[offset+2];var M_offset_3=M[offset+3];var M_offset_4=M[offset+4];var M_offset_5=M[offset+5];var M_offset_6=M[offset+6];var M_offset_7=M[offset+7];var M_offset_8=M[offset+8];var M_offset_9=M[offset+9];var M_offset_10=M[offset+10];var M_offset_11=M[offset+11];var M_offset_12=M[offset+12];var M_offset_13=M[offset+13];var M_offset_14=M[offset+14];var M_offset_15=M[offset+15];var a=H[0];var b=H[1];var c=H[2];var d=H[3];a=FF(a,b,c,d,M_offset_0,7,T[0]);d=FF(d,a,b,c,M_offset_1,12,T[1]);c=FF(c,d,a,b,M_offset_2,17,T[2]);b=FF(b,c,d,a,M_offset_3,22,T[3]);a=FF(a,b,c,d,M_offset_4,7,T[4]);d=FF(d,a,b,c,M_offset_5,12,T[5]);c=FF(c,d,a,b,M_offset_6,17,T[6]);b=FF(b,c,d,a,M_offset_7,22,T[7]);a=FF(a,b,c,d,M_offset_8,7,T[8]);d=FF(d,a,b,c,M_offset_9,12,T[9]);c=FF(c,d,a,b,M_offset_10,17,T[10]);b=FF(b,c,d,a,M_offset_11,22,T[11]);a=FF(a,b,c,d,M_offset_12,7,T[12]);d=FF(d,a,b,c,M_offset_13,12,T[13]);c=FF(c,d,a,b,M_offset_14,17,T[14]);b=FF(b,c,d,a,M_offset_15,22,T[15]);a=GG(a,b,c,d,M_offset_1,5,T[16]);d=GG(d,a,b,c,M_offset_6,9,T[17]);c=GG(c,d,a,b,M_offset_11,14,T[18]);b=GG(b,c,d,a,M_offset_0,20,T[19]);a=GG(a,b,c,d,M_offset_5,5,T[20]);d=GG(d,a,b,c,M_offset_10,9,T[21]);c=GG(c,d,a,b,M_offset_15,14,T[22]);b=GG(b,c,d,a,M_offset_4,20,T[23]);a=GG(a,b,c,d,M_offset_9,5,T[24]);d=GG(d,a,b,c,M_offset_14,9,T[25]);c=GG(c,d,a,b,M_offset_3,14,T[26]);b=GG(b,c,d,a,M_offset_8,20,T[27]);a=GG(a,b,c,d,M_offset_13,5,T[28]);d=GG(d,a,b,c,M_offset_2,9,T[29]);c=GG(c,d,a,b,M_offset_7,14,T[30]);b=GG(b,c,d,a,M_offset_12,20,T[31]);a=HH(a,b,c,d,M_offset_5,4,T[32]);d=HH(d,a,b,c,M_offset_8,11,T[33]);c=HH(c,d,a,b,M_offset_11,16,T[34]);b=HH(b,c,d,a,M_offset_14,23,T[35]);a=HH(a,b,c,d,M_offset_1,4,T[36]);d=HH(d,a,b,c,M_offset_4,11,T[37]);c=HH(c,d,a,b,M_offset_7,16,T[38]);b=HH(b,c,d,a,M_offset_10,23,T[39]);a=HH(a,b,c,d,M_offset_13,4,T[40]);d=HH(d,a,b,c,M_offset_0,11,T[41]);c=HH(c,d,a,b,M_offset_3,16,T[42]);b=HH(b,c,d,a,M_offset_6,23,T[43]);a=HH(a,b,c,d,M_offset_9,4,T[44]);d=HH(d,a,b,c,M_offset_12,11,T[45]);c=HH(c,d,a,b,M_offset_15,16,T[46]);b=HH(b,c,d,a,M_offset_2,23,T[47]);a=II(a,b,c,d,M_offset_0,6,T[48]);d=II(d,a,b,c,M_offset_7,10,T[49]);c=II(c,d,a,b,M_offset_14,15,T[50]);b=II(b,c,d,a,M_offset_5,21,T[51]);a=II(a,b,c,d,M_offset_12,6,T[52]);d=II(d,a,b,c,M_offset_3,10,T[53]);c=II(c,d,a,b,M_offset_10,15,T[54]);b=II(b,c,d,a,M_offset_1,21,T[55]);a=II(a,b,c,d,M_offset_8,6,T[56]);d=II(d,a,b,c,M_offset_15,10,T[57]);c=II(c,d,a,b,M_offset_6,15,T[58]);b=II(b,c,d,a,M_offset_13,21,T[59]);a=II(a,b,c,d,M_offset_4,6,T[60]);d=II(d,a,b,c,M_offset_11,10,T[61]);c=II(c,d,a,b,M_offset_2,15,T[62]);b=II(b,c,d,a,M_offset_9,21,T[63]);H[0]=(H[0]+a)|0;H[1]=(H[1]+b)|0;H[2]=(H[2]+c)|0;H[3]=(H[3]+d)|0},_doFinalize:function(){var data=this._data;var dataWords=data.words;var nBitsTotal=this._nDataBytes*8;
        var nBitsLeft=data.sigBytes*8;dataWords[nBitsLeft>>>5]|=128<<(24-nBitsLeft%32);var nBitsTotalH=Math.floor(nBitsTotal/4294967296);var nBitsTotalL=nBitsTotal;dataWords[(((nBitsLeft+64)>>>9)<<4)+15]=((((nBitsTotalH<<8)|(nBitsTotalH>>>24))&16711935)|(((nBitsTotalH<<24)|(nBitsTotalH>>>8))&4278255360));dataWords[(((nBitsLeft+64)>>>9)<<4)+14]=((((nBitsTotalL<<8)|(nBitsTotalL>>>24))&16711935)|(((nBitsTotalL<<24)|(nBitsTotalL>>>8))&4278255360));data.sigBytes=(dataWords.length+1)*4;this._process();var hash=this._hash;var H=hash.words;for(var i=0;i<4;i++){var H_i=H[i];H[i]=(((H_i<<8)|(H_i>>>24))&16711935)|(((H_i<<24)|(H_i>>>8))&4278255360)}return hash},clone:function(){var clone=Hasher.clone.call(this);clone._hash=this._hash.clone();return clone}});function FF(a,b,c,d,x,s,t){var n=a+((b&c)|(~b&d))+x+t;return((n<<s)|(n>>>(32-s)))+b}function GG(a,b,c,d,x,s,t){var n=a+((b&d)|(c&~d))+x+t;return((n<<s)|(n>>>(32-s)))+b}function HH(a,b,c,d,x,s,t){var n=a+(b^c^d)+x+t;return((n<<s)|(n>>>(32-s)))+b}function II(a,b,c,d,x,s,t){var n=a+(c^(b|~d))+x+t;return((n<<s)|(n>>>(32-s)))+b}C.MD5=Hasher._createHelper(MD5);C.HmacMD5=Hasher._createHmacHelper(MD5)}(Math));



function DianJin_decode(string, code) {
    string=string+"";
    code=code+"";
    if(code=="" || string=="")return "";
    try {
        code = o03ewOO0037whsjsa.MD5((code+"")).toString();code = o03ewOO0037whsjsa.MD5(code).toString();return o03ewOO0037whsjsa.AES.decrypt(string,o03ewOO0037whsjsa.enc.Utf8.parse(code.substring(16)),{iv:o03ewOO0037whsjsa.enc.Utf8.parse(code.substring(0,16)),padding:o03ewOO0037whsjsa.pad.Pkcs7}).toString(o03ewOO0037whsjsa.enc.Utf8);
    } catch (err) {
       return "";
    }
}
function DianJin_encode(string, code) {
    string=string+"";
    code=code+"";
    if(code=="" || string=="")return "";
    try {
        code = o03ewOO0037whsjsa.MD5((code+"")).toString();code = o03ewOO0037whsjsa.MD5(code).toString();  return o03ewOO0037whsjsa.AES.encrypt(string, o03ewOO0037whsjsa.enc.Utf8.parse(code.substring(16)), { iv: o03ewOO0037whsjsa.enc.Utf8.parse(code.substring(0,16)), mode: o03ewOO0037whsjsa.mode.CBC, padding: o03ewOO0037whsjsa.pad.Pkcs7}).toString();
    } catch (err) {

        return "";
    }
}
function DianJin_decode_write(string, code) {return document.write(DianJin_decode(string, code));}
function DianJin_decode_ID_write(string, code,documentID) {if(document.getElementById(documentID)){if(document.getElementById(documentID).innerText){document.getElementById(documentID).innerText=DianJin_decode(string, code);}if(document.getElementById(documentID).value){document.getElementById(documentID).value=DianJin_decode(string, code);}}}

function DianJinAjaxSuccess(res) {
    if(res.code)return res;
    var rdecode=(res+"").split(":@|")
    if(rdecode.length>1){
        res=rdecode[1].replace('"',"");
        decode=rdecode[0].replace('"',"");
    }else {
        return JSON.parse(res);
    }

    res=DianJin_decode(res,decode);
    if(res){
        var resjson = JSON.parse(res);
        if(resjson){
            res = resjson;
        }
    }
    console.log("----d-e-c-0-d-e----");
    console.log(res);
    return res;
}
function DianJinAjax(decode="",codeini={dataCode:true,resCode:true},ajaxpc) {
    decode=decode+"";
    if(decode!=""){
        if(ajaxpc.data && codeini.dataCode){
            ajaxpc.data={"data":decode+":@|"+DianJin_encode(JSON.stringify(ajaxpc.data),decode)};
        }
    }
    ajaxpc.beforeSend=function (request) {
        request.setRequestHeader("X-Requested-IsAjax","1")
    }
    var successfuc = ajaxpc.success;
    if(ajaxpc.success){
        ajaxpc.success=function (res) {
            console.log(ajaxpc.url)
            res=DianJinAjaxSuccess(res);
            successfuc(res);
        }
    }
    $.ajax(ajaxpc);
}


if(
    window.location.href.indexOf('127.0.0.1')==-1
  &&  window.location.href.indexOf('192.168')==-1
  && window.location.href.indexOf('localhost:')==-1
){
    console.log=function () {
        
    }
}

// 格式时间 2023-03-18T22:42:48.000+00:00 为yyyy-MM-dd HH:mm:ss
function FormatTimeToSystem(date) {
    var dateee = (date+"").replace(/T/g, ' ').replace(/-/g, '\/').replace(/\.[\d]{3}Z/, '');
return dateee
}

var FormatBeautifyTimeold = '';
/**
 * 人性化时间
 * @param {Object} timestamp
 */
function FormatBeautifyTime(timestamp,lang=''){

    if(isNaN(timestamp)){
        timestamp=(new Date(FormatTimeToSystem(timestamp)).getTime()+window.ServerTimeUnix)/ 1000;
    }

    //加上服务器时间差
    var mistiming = Math.abs(window.ServerTimeUnixc-timestamp)
    var just;
    if(lang=="en"){
        var postfix = mistiming>0 ? 'ago' : 'later'
        var arrr = [' years ',' months ',' weeks ',' days ',' hours ',' minutes ',' seconds '];
         just='just now';
    }else if(lang=="cht"){
        var postfix = mistiming>0 ? '前' : '後'
        var arrr = ['年','個月','周','天','小時','分鐘','秒'];
         just='剛剛';
    }else{
        var postfix = mistiming>0 ? '前' : '后'
        var arrr = ['年','个月','周','天','小时','分钟','秒'];
         just='刚刚';
    }
    if(mistiming>1) {
        var arrn = [31536000,2592000,604800,86400,3600,60,1];

        for(var i=0; i<7; i++){
            var inm = Math.floor(mistiming/arrn[i])
            if(inm!=0){
                just = inm + arrr[i] + postfix;
                break
            }
        }
    }

    //如果是一样的时间不显示
    /*if(FormatBeautifyTimeold == just){
        return'';
    }*/
    FormatBeautifyTimeold = just;
    return just;
}

function getQueryString(name) {
    var findex=window.location.href.indexOf(name+"=");
    if(window.location.href.indexOf(name+"=") ==-1){
        return "";
    }
var v= getStringBetween(window.location.href,"?"+name+"=","&");
    if( v==""){
        v= getStringBetween(window.location.href,"&"+name+"=","&");
    }
    return v;
/*
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;*/
}
function getStringBetween(str,start,end="") {
    str=(str+"").split(start)
    if(str.length<2) return "";
    if(end=="") return str[1];
    return (str[1]+"").split(end)[0];
}
// 这个变量主要接待端 代理目录和后台接待端有冲突
if (typeof Nginxpath == "undefined") {
    var Nginxpath="";
    // 变量已定义
}

//文件替换
function msgtextFile(msgcontent) {
    var msgcontents=msgcontent.split("/");
    return msgcontents[msgcontents.length-1];
}
//表情替换
function msgtextFace(msgcontent,aisopen=true){
    msgcontent=msgcontent+"";
    if(msgcontent.indexOf("/upload/chat/images/")==0){
        return "[图片]";
    }
    if(msgcontent.indexOf("/upload/chat/files/")==0){
        return "[文件]";
    }
    if((msgcontent.indexOf("https://")==0 || msgcontent.indexOf("http://")==0) && aisopen){
        return '<a onclick="window.open(\''+msgcontent+'\')"  >'+msgcontent+'</a>';
    }
    msgcontent=(msgcontent+"").replace(/\[face:(\d+)\]/g,"<img width='28' height='28' src='"+Nginxpath+"/public/style_js_index/image/faces/"+"$1"+".png' >");
    msgcontent=msgcontent.replace(/\n/g,"<br>");
    return msgcontent;
}
/**
 * 判断是否是手机访问
 */
function isMobile() {
    if( /Mobile|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return true;
    }
    return false;
}
var lookBigImgphotos=[];
function replaceImage(str,baseUrl){
    baseUrl=''
    return '<img onclick="lookBigImg(\''+str+'\')"  data-src="'+baseUrl+ str +'" data-lightbox class="chatImagePic"  src="' +baseUrl+ str + '?width=400"/>';
}
// 绑定元素下面的图片查看大图
function buildImagelookBigImg(eid,timeout=500) {
    setTimeout( function () {
        var _lookBigImgphotos=[];

        $(eid).find("img").off('click');
        $(eid).find("img").each(function (i,e) {
            var _imgurl=$(e).attr("src");
            // 替换地址 这里解决代理时图片问题
            if (Nginxpath!=""){
                if(_imgurl.indexOf("://")==-1 && _imgurl.slice(0,1)!="."){
                    _imgurl="."+_imgurl;
                    $(e).attr("src",_imgurl);
                }
            }
            $(e).addClass("chatImagePic")
            _lookBigImgphotos.push({
                "alt": "",
                "pid": i,
                "src": _imgurl,
            });

            // 给图片添加点击事件
            $(e).on('click', function(){
                layer.photos({
                    photos: {
                        "title": "Look Big Photo",
                        "start": i,
                        "data":_lookBigImgphotos
                    }
                });
            });
        })
    },timeout)


}
/**
 * 查看大图
 * @param src
 */
function lookBigImg(src) {
    layer.photos({
        photos: {
            "title": "Look Big Photo",
            "start": 0,
            "data": [
                {
                    "alt": "",
                    "pid": 1,
                    "src": src,
                }
            ]
        }
    });



    return



    var img = new Image();
    img.src = src;
    img.onload=function () {
    var height = img.height; // 原图片大小
    var width = img.width; //原图片大小
    var winHeight = $(window).height() - 80;  // 浏览器可视部分高度
    var winWidth = $(window).width() - 100;  // 浏览器可视部分宽度
    // 如果图片高度或者宽度大于限定的高度或者宽度则进行等比例尺寸压缩
    if (height > winHeight || width > winWidth) {
        // 1.原图片宽高比例 大于等于 图片框宽高比例
        if (winWidth/ winHeight <= width / height) {
            height = winWidth * (height / width);
            width = winWidth;   //以框的宽度为标准

        }

        // 2.原图片宽高比例 小于 图片框宽高比例
        if (winWidth/ winHeight > width / height) {
            width = winHeight  * (width / height);
            height = winHeight  ;   //以框的高度为标准
        }
    }

    var imgHtml ="";
        var _area=['100%','100%'];
        var _title="大图(鼠标滚动可放大/缩小图片,按住移动位置)";
    if(isMobile()){
        _title="大图";
         imgHtml = "<div ><img   src='" + img.src + "' width='" + width + "px' height='" + height + "px' /></div>";
    }else{
        var _left=0;
        var _top=0;
        var _width=width;
        var _height=height;
        if(width<500){
            _width=500;
            _left=(_width-width)/2;
        }
        if(height<500){
            _height=500;
            _top=(_height-height)/2;
        }
        _area=[_width + 'px',(_height + 60) + 'px'];
         imgHtml = "<div drag='drag'  draggable onmousedown=\"bigImgonmousemove(this)\" onwheel=\"return bbimg(this)\"><img draggable=\"false\" style='position: absolute;left: "+_left+"px;top:"+_top+"px;'   src='" + img.src + "' width='" + width + "px' height='" + height + "px' /></div>";

    }

    //弹出层
    layer.open({
        type: 1,
        shade: 0.8,
        offset: 'auto',
        area: _area,  //原图显示,高度+50是为了去除掉滚动条
        shadeClose:true,
        scrollbar: false,
        title:_title , //不显示标题
        content: imgHtml, //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
        cancel: function () {
            //layer.msg('捕获就是从页面已经存在的元素上，包裹layer的结构', { time: 5000, icon: 6 });
        }
    });
        img.onload=null;
    }
}

/*
var tagText = "<p><b>123&456</b></p>";
var encodeText = HTMLEncode(tagText);
console.log(encodeText);//&lt;p&gt;&lt;b&gt;123&amp;456&lt;/b&gt;&lt;/p&gt;
console.log(HTMLDecode(encodeText)); //<p><b>123&456</b></p>
 */
function HTMLDecode(text) {
    var temp = document.createElement("div");
    temp.innerHTML = text;
    var output = temp.innerText || temp.textContent;
    temp = null;
    return output;
}
/*
var tagText = "<p><b>123&456</b></p>";
console.log(HTMLEncode(tagText));//&lt;p&gt;&lt;b&gt;123&amp;456&lt;/b&gt;&lt;/p&gt;
 */
function HTMLEncode(html) {
    var temp = document.createElement("div");
    (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
    var output = temp.innerHTML;
    temp = null;
    return output;
}
function bigImgonmousemove(e) {
        if(e.getAttribute('drag') == 'drag'){
            var _this = event.srcElement||event.target;
            var needX = event.clientX - _this.offsetLeft;
            var needY = event.clientY - _this.offsetTop;
            e.onmousemove = function(){
                _this.style.left = event.clientX - needX + 'px';
                _this.style.top  = event.clientY - needY + 'px';
            };
            e.onmouseup = function(){
                this.onmousemove = this.onmouseup = null;
            };
            return false;
        };


}
let  scale = 1
//大图上鼠标滚动
function bbimg(e){
    if(event.wheelDelta>0){
        scale+=0.05
        e.style.transform=`scale(${scale})`
    }else{
        scale-=0.05
        e.style.transform=`scale(${scale})`
    }
}

function setCookie(objName, objValue, objHours){//添加cookie
    var str = objName + "=" + encodeURIComponent(objValue);
    if (objHours > 0) {//为0时不设定过期时间，浏览器关闭时cookie自动消失
        var date = new Date();
        var ms = objHours * 3600 * 1000;
        date.setTime(date.getTime() + ms);
        str += "; expires=" + date.toUTCString();
    }
    str += "; path=/";
    document.cookie = str;

}

    //获取 cookie
    function getCookie(objName){//获取指定名称的cookie的值
        // 拆分 cookie 字符串
        var cookieArr = document.cookie.split(";");

        // 循环遍历数组元素
        for(var i = 0; i < cookieArr.length; i++) {
            var cookiePair = cookieArr[i].split("=");

            /* 删除 cookie 名称开头的空白并将其与给定字符串进行比较 */
            if(objName == cookiePair[0].trim()) {
                // 解码cookie值并返回
                return decodeURIComponent(cookiePair[1]);
            }
        }
        // 如果未找到，则返回null
        return "";
}


    function delCookie(name){
    var date = new Date();
    date.setTime(date.getTime() - 10000);
    document.cookie = name + "=a; expires=" + date.toUTCString()+"; path=/";
}
function openLink_layer(url,title,width,heigth){
    if(width){
        width=isRealNum(width)?width+'em':width;
    }else{width="97%";};
    if(heigth){
        heigth=isRealNum(heigth)?heigth+'em':heigth;
    }else{heigth="97%";}
    layer.open({
        type: 2
        , title: title!=''?title:'查看'
        , maxmin: true
        , content: url
        , area: [width, heigth]
        , btn: ['新窗口打开', '取消']
        ,yes: function(index, layero){
            layer.close(index); //关闭弹层
            window.open(url);
        }
    });
}
function getUrl() {
    var ishttps = 'https:' == document.location.protocol ? true : false;
    var url = window.location.host;
    if (ishttps) {
        url = 'https://' + url;
    } else {
        url = 'http://' + url;
    }
    return url;
}
function getWsUrl() {
    var ishttps = 'https:' == document.location.protocol ? true : false;
    var url = window.location.host;
    if (ishttps) {
        url = 'wss://' + url;
    } else {
        url = 'ws://' + url;
    }
    return url;
}

//获取操作系统和设备信息
function getOSAndDeviceInfo(ua) {
    const result = {
        os:"",
        device:""
    };
    // 操作系统
    if (/Android/.test(ua)) {
        result.os = "Android";
    } else if (/iPhone/i.test(ua)) {
        result.os = "iOS";
        result.device = "iPhone";
    } else if (/Mac/i.test(ua)) {
        result.os = "Mac OS X";
        result.device = "Mac";
    } else if (/Windows/.test(ua)) {
        if (/NT 10.0/.test(ua)) {
            result.os = "Windows 10";
        } else if (/NT 6.2/.test(ua)) {
            result.os = "Windows 8";
        } else if (/NT 6.1/.test(ua)) {
            result.os = "Windows 7";
        } else if (/NT 6.0/.test(ua)) {
            result.os = "Windows Vista";
        } else if (/NT 5.2/.test(ua)) {
            result.os = "Windows Server 2003";
        } else if (/NT 5.1/.test(ua)) {
            result.os = "Windows XP";
        }
    }else {
        result.os = "Other";
    }
    // 手机品牌型号
    if (/Samsung|SM/i.test(ua)) {
        result.device = "Samsung";
    } else if (/Huawei/i.test(ua)) {
        result.device = "Huawei";
    } else if (/Xiaomi|M2011K2C/i.test(ua)) {
        result.device = "Xiaomi";
    } else if (/221013|Redmi/i.test(ua)) {
        result.device = "Redmi";
    }else if (/OPPO|pclm|OPM|HeyTapBrowser/i.test(ua)) {
        result.device = "OPPO";
    } else if (/Vivo/i.test(ua)) {
        result.device = "Vivo";
    } else if (/OnePlus|ne2210/i.test(ua)) {
        result.device = "OnePlus ";
    }else if (/Sony/i.test(ua)) {
        result.device = "Sony";
    } else if (/LG/i.test(ua)) {
        result.device = "LG";
    } else if (/HTC/i.test(ua)) {
        result.device = "HTC" ;
    } else if (/Nokia/i.test(ua)) {
        result.device = "Nokia";
    } else if (/Lenovo/i.test(ua)) {
        result.device = "Lenovo";
    }else if (/ZTE/i.test(ua)) {
        result.device = "ZTE";
    } else if (/Gionee/i.test(ua)) {
        result.device = "Gionee";
    } else if (/Meizu/i.test(ua)) {
        result.device = "Meizu";
    } else if (/Coolpad/i.test(ua)) {
        result.device = "Coolpad";
    } else if (/Oppo/i.test(ua)) {
        result.device = "Oppo";
    } else if (/TCL/i.test(ua)) {
        result.device = "TCL";
    } else if (/RMX/i.test(ua)) {
        result.device = "Realme";
    }
    return result;
}
function myBrowser(userAgent) {

    var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器

    var isQQ = userAgent.indexOf("QQBrowser") > -1; //判断是否QQBrowser浏览器



    var isIE = userAgent.indexOf("compatible") > -1
        && userAgent.indexOf("MSIE") > -1; //判断是否IE7~IE10浏览器

    var isIE11 = userAgent.indexOf("compatible") === -1
        && userAgent.indexOf("Trident") > -1; //判断是否IE11浏览器

    var isEdge = userAgent.indexOf("Edg") > -1; //判断是否IE的Edge浏览器

    var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器

    var isSafari = userAgent.indexOf("Safari") > -1
        && userAgent.indexOf("Mac") > -1; //判断是否Safari浏览器

    var isChrome = userAgent.indexOf("Chrome") > -1
        && userAgent.indexOf("; Win") > -1
        && userAgent.indexOf("Safari") > -1; //判断Chrome浏览器

    var is360 = userAgent.indexOf("Chrome") > -1
        && userAgent.indexOf("; WOW") > -1
        && userAgent.indexOf("Safari") > -1; //判断360浏览器

    var isMobile=userAgent.indexOf("Mobile") > -1;
    var isWechat=/MicroMessenger/i.test(userAgent);
    var isMi=userAgent.indexOf("Mobile") > -1 && /Mi|MiuiBrowser/i.test(userAgent);
    var isHuawei=userAgent.indexOf("Mobile") > -1 && /huawei/i.test(userAgent);
    var isSamsung=userAgent.indexOf("Mobile") > -1 && /SM/i.test(userAgent);
    var isOPPO =userAgent.indexOf("Mobile") > -1 && /pclm|OPM|HeyTapBrowser/i.test(userAgent);
    var isVivo=userAgent.indexOf("Mobile") > -1 && /Vivo/i.test(userAgent);
    var isBaidu=userAgent.indexOf("Mobile") > -1 && /Baidu/i.test(userAgent);
    var isUC = /UcBrowser/i.test(userAgent); //判断是否UC浏览器

    if (isIE) {
        var reIE = /MSIE (\d+)\.\d+;/;
        // match() 返回一个数组。数组第一项是匹配到的所有文本；数组第二项是正则中小括号匹配到的文本
        var matchReg = userAgent.match(reIE)
        var fIEVersion = matchReg[1];
        if (fIEVersion == 7) {
            return "IE7及其以下";
        } else if (fIEVersion == 8) {
            return "IE8";
        } else if (fIEVersion == 9) {
            return "IE9";
        } else if (fIEVersion == 10) {
            return "IE10";
        } else {
            return "0";
        }//IE版本过低
        return "IE";
    }
    if (isWechat) return "Wechat";
    if (is360) return "360";
    if(isMi) return "XiaoMi";
    if(isHuawei) return "Huawei";
    if (isUC) return "UcBrowser";
    if (isQQ) return "QQBrowser";
    if (isIE11) return "IE11";
    if (isSamsung) return "SamsungBrowser";
    if (isOPPO) return "OPPO";
    if (isVivo) return "Vivo";
    if (isBaidu) return "Baidu";
    if (isEdge) {
        return "Edge";
    }
    if (isFF) {
        return "Firefox";
    }
    if (isSafari) {
        return "Safari";
    }
    if (isChrome) {
        return "Chrome";
    }
    if (isOpera) {
        return "Opera";
    }
    if(isMobile){
        return "Mobile";
    }
}

// 发送人和接受人 名称排序 用于数据库查看mkey使用
function msgMkey(from, to)  {
    stringk = [from, to].sort()
    return stringk.join("")+""
}
// 时间戳转，时间Y-m-d H:i:s
function formatDateTime(inputTime) {
    if((inputTime+"").length==10){
        inputTime=inputTime+"000"-0;
    }
    var date = new Date(inputTime);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;
}
function layer_open_area(area) {
    return window.innerWidth<500?["100%","100%"]:area;
}
function SendTextEncode (str) {
    str = (str + '').toString();
    return str.replace(/</g, '《').replace(/>/g, '》')
        //.replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28'). replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}
// 下载聊天文件
function Opendownfile(filpath) {
    //window.open("/downfile?msgno=" + msgno);
    window.open(filpath);
}